/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */
#include "zirang.h"

bool8
AreAABBOverlapping(V3 minPos0, V3 maxPos0, V3 minPos1, V3 maxPos1)
{
    if(minPos0.x < maxPos1.x && maxPos0.x > minPos1.x &&
       minPos0.y < maxPos1.y && maxPos0.y > minPos1.y &&
       minPos0.z < maxPos1.z && maxPos0.z > minPos1.z)
    {
        return 1;
    }
    
    return 0;
}

bool8
IsPointInsideAABB(V3 minPos, V3 maxPos, V3 point)
{
    if(point.x > minPos.x && point.x < maxPos.x &&
       point.y > minPos.y && point.y < maxPos.y &&
       point.z > minPos.z && point.z < maxPos.z)
    {
        return 1;
    }

    return 0;
}

void
GetAABBVertices(V3 minPos, V3 maxPos, V3 *outVertices)
{
    outVertices[0] = v3(minPos.x, minPos.y, minPos.z);
    outVertices[1] = v3(minPos.x, maxPos.y, minPos.z);
    outVertices[2] = v3(maxPos.x, maxPos.y, minPos.z);
    outVertices[3] = v3(maxPos.x, minPos.y, minPos.z);

    outVertices[4] = v3(minPos.x, minPos.y, maxPos.z);
    outVertices[5] = v3(minPos.x, maxPos.y, maxPos.z);
    outVertices[6] = v3(maxPos.x, maxPos.y, maxPos.z);
    outVertices[7] = v3(maxPos.x, minPos.y, maxPos.z);
}

#if ZIRANG_DEBUG

LoadedBitmap
GenerateExplosionParticleTex(MemoryArena *arena, uint32 width, uint32 height, uint32 bitsPerPixel)
{
    LoadedBitmap result = {};
    result.width = width;
    result.height = height;
    result.bitsPerPixel = bitsPerPixel;
    
    uint32 bytesPerPixel = bitsPerPixel / 8;
    uint32 totalBytes = width * height * sizeof(Color);
    result.data = (Color *)PushBuffer(arena, totalBytes);

    Color *pixels = result.data;
    Color color0 = { 216, 49, 91, 255 };
    Color color1 = { 62, 146, 204, 255 };
    
    uint32 radius = width / 2;
    uint32 r2 = radius * radius;
    V2 centre = { width / 2.0f, height / 2.0f };
    real32 average = (width+height)/2.0f;
    for(uint32 row = 0; row < height; row++)
    {
        for(uint32 col = 0; col < width; col++)
        {
            if((row+col) < average)
                *pixels++ = color0;
            else
                *pixels++ = color0;
        }
    }
    
    return result;
}

LoadedBitmap
GenerateColorTex(MemoryArena *arena, uint32 width, uint32 height, uint32 bitsPerPixel)
{
    LoadedBitmap result = {};
    result.width = width;
    result.height = height;
    result.bitsPerPixel = bitsPerPixel;
    
    uint32 bytesPerPixel = bitsPerPixel / 8;
    uint32 totalBytes = width * height * sizeof(Color);
    result.data = (Color *)PushBuffer(arena, totalBytes);

    Color *pixels = result.data;
    Color color0 = { 216, 49, 91, 255 };
    Color color1 = { 62, 146, 204, 255 };
    
    uint32 radius = width / 2;
    uint32 r2 = radius * radius;
    V2 centre = { width / 2.0f, height / 2.0f };
    real32 average = (width+height)/2.0f;
    for(uint32 row = 0; row < height; row++)
    {
        for(uint32 col = 0; col < width; col++)
        {
            if((row+col) < average)
                *pixels++ = color0;
            else
                *pixels++ = color1;
        }
    }
    
    return result;
}

#endif

bool8
IsHover(UIElement uiElement, uint32 cursorX, uint32 cursorY)
{
    uint32 maxX = uiElement.leftX + uiElement.width - 1;
    uint32 maxY = uiElement.bottomY + uiElement.height - 1;
    if(cursorX >= uiElement.leftX && cursorX <= maxX && cursorY >= uiElement.bottomY && cursorY <= maxY)
    {
        return 1;
    }

    return 0;
}

#if ZIRANG_DEBUG
GameMemory *debugGameMemory;
#endif

void
OnSteerLeft()
{
}

void
OnSteerRight()
{
}

bool8
isOver(UIElement uiElement, real32 cursorX, real32 cursorY)
{
    uint32 minX = uiElement.leftX;
    uint32 minY = uiElement.bottomY;
    uint32 maxX = minX + uiElement.width;
    uint32 maxY = minY + uiElement.height;
    bool8 result = 0;
    if(cursorX > minX && cursorX < maxX && cursorY > minY && cursorY < maxY)
    {
        result = 1;
    }
    return result;
}

#define USER_INTERFACE 0

void
MainMenuScreenUpdate(GameData gameData)
{
    GameBuffer *buffer = gameData.buffer;
    GameMemory *memory = gameData.memory;
    GameInput *input = gameData.input;
    Game *game = (Game *)memory->permanentMemory;

    if(input->buttons.left.isDown)
    {
        game->Update = GameScreenUpdate;
    }
/*
    RenderBuffer renderBuffer = {};
    renderBuffer.width = buffer->width;
    renderBuffer.height = buffer->height;
    renderBuffer.pixels = (uint32 *)buffer->pixels;
    renderBuffer.zBuffer = (real32 *)game->zBuffer;

    ClearZBuffer(game->zBuffer, 1.0f, renderBuffer.width, renderBuffer.height);
    Color bgColor = Color(128, 64, 32, 255);
    ClearBuffer(renderBuffer.pixels, renderBuffer.width, renderBuffer.height, bgColor);
*/
}

void
GameScreenUpdate(GameData gameData)
{
    GameBuffer *buffer = gameData.buffer;
    GameMemory *memory = gameData.memory;
    GameTiming timing = gameData.timing;
    GameInput *input = gameData.input;
    //PlatformFunctions platform = gameData.platform;
    Game *game = (Game *)memory->permanentMemory;

    real32 deltaTime = timing.deltaTime;
    real32 deltaTimeMs = timing.deltaTimeMs;
    uint32 bufferWidth = buffer->width;
    uint32 bufferHeight = buffer->height;
    uint32 bytesPerPixel = buffer->bytesPerPixel;
    uint32 *pixels = buffer->pixels;
    //Color *colorBuffer0 = game->colorBuffer0;
    //Color *colorBuffer1 = game->colorBuffer1;
    real32 *zBuffer = game->zBuffer;
    ColorBuffer *colorBuffers = game->colorBuffers;
    real32 aspectRatio = (real32)bufferWidth / (real32)bufferHeight;
    uint32 totalPixels = bufferWidth * bufferHeight;

    MemoryArena *tempArena = &game->tempArena;
    MemoryArena *permArena = &game->permArena;
    Model3D fighterJetModel = game->fighterJetModel;
    Camera camera = game->camera;
    FighterJet playerJet = game->playerJet;

    if(playerJet.alive)
    {
        // NOTE: User Input.
        if(input->isController)
        {
            real32 dXPos = playerJet.tiltMax / (real32)SHRT_MAX;
            real32 dXNeg = playerJet.tiltMax / (real32)SHRT_MIN;
            int16 lStickX = input->controller.lAnalogX;

            if(lStickX < 0)
            {
                playerJet.tiltAngle = (dXNeg * lStickX);
            }
            else if(lStickX > 0)
            {
                playerJet.tiltAngle = (-dXPos * lStickX);
            }
            else
            {
                playerJet.tiltAngle /= playerJet.tiltDampener;
                if(Abs(playerJet.tiltAngle) < 1.0f)
                    playerJet.tiltAngle = 0.0f;
            }
        }
        else
        {
            if(input->buttons.left.isDown)
            {
                if(playerJet.tiltAngle < 0.0f)
                    playerJet.tiltAngle /= playerJet.tiltDampener;
                playerJet.tiltAngle += (playerJet.tiltSpeed * deltaTime);
            }
            else if(input->buttons.right.isDown)
            {
                if(playerJet.tiltAngle > 0.0f)
                    playerJet.tiltAngle /= playerJet.tiltDampener;
                playerJet.tiltAngle -= (playerJet.tiltSpeed * deltaTime);
            }
            else
            {
                playerJet.tiltAngle /= playerJet.tiltDampener;
                if(Abs(playerJet.tiltAngle) < 1.0f)
                    playerJet.tiltAngle = 0.0f;
            }
            
        }
        
#if USER_INTERFACE
        UIElement *uiElements = game->uiElements;
        for(uint32 i = 0; i < GameScreenUI_Count; i++)
        {
            if(isOver(uiElements[i], input->mouseInput.newPosX, input->mouseInput.newPosY))
            {                
                if(input->mouseInput.isPressed)
                {
                    uiElements[i].color = Color(255, 0, 0, 255);
                    game->uiElements[i].onClick();
                }
                else
                {
                    uiElements[i].color = Color(128, 255);
                }
            }
            else
            {
                uiElements[i].color = Color(255, 255);
            }
        }
#endif
        
        // NOTE: Movement and orientation calculation of the jet. Both
        // depend solely on its "tiltAngle".
        playerJet.tiltAngle = Clamp(playerJet.tiltAngle, -playerJet.tiltMax, playerJet.tiltMax);
        V3 newLiftForce = RotatePoint(playerJet.liftForce, -playerJet.forward,
                                      playerJet.tiltAngle) * playerJet.maneuverMultiplier;
        V3 liftSideForce = playerJet.right * Dot(newLiftForce, playerJet.right);
        V3 liftRightAcceleration = liftSideForce / playerJet.mass;
        V3 newVelocity = playerJet.velocity + (liftRightAcceleration * deltaTime);
        if(playerJet.moveSpeed > 0.0f)
        {
            playerJet.forward = Normalize(newVelocity);
            playerJet.right = Cross(playerJet.forward, playerJet.up);
        }
        playerJet.velocity = playerJet.forward * playerJet.moveSpeed;
        playerJet.transform.position += (playerJet.velocity * deltaTime);
    
        V3 worldForward = v3(0.0f, 0.0f, -1.0f);
        real32 angleY = RadToDeg(acos(Dot(worldForward, playerJet.forward)));
        if(playerJet.forward.x < 0.0f)
            playerJet.transform.eulerAngles.y = angleY;
        else
            playerJet.transform.eulerAngles.y = -angleY;
        playerJet.transform.eulerAngles.z = playerJet.tiltAngle;

        game->explosionEmitter.transform = Transform(playerJet.transform.position + (5.0f * playerJet.forward),
                                                     v3(0.0f), v3(1.0f));
        
    }

    // TODO: Verify logic (asteroid generation and simulation) via
    // visualization.  NOTE: Asteroid generation and simulation. We
    // always keep the ones that are active, first in the array.
    AsteroidGenerator *generator = &game->asteroidGenerator;
    V3 boundsDelta = v3(generator->boundSize / 2.0f, generator->boundSize / 2.0f,
                        generator->boundSize / 1.0f);
    generator->minBound = playerJet.transform.position - boundsDelta;
    generator->maxBound = playerJet.transform.position + boundsDelta;
    V3 minBound = generator->minBound;
    V3 maxBound = generator->maxBound;

    uint32 activeIndex = 0;
    uint32 totalActive = generator->totalActive;

#if DUMMY_ASTEROID
    V3 lightPosition = playerJet.transform.position - (2.0f * game->light.direction);
#endif

    // NOTE: Simulating the asteroids that are already active.
    for(uint32 i = 0; i < totalActive; i++)
    {
        Asteroid asteroid = generator->asteroids[i];
#if DUMMY_ASTEROID
        asteroid.transform.position = camera.transform.position + v3(0, 0, -1.51f);
#else
        asteroid.transform.position += (asteroid.direction * asteroid.speed * deltaTime);
        asteroid.currentRotationAngle += (asteroid.rotationSpeed * deltaTime);
#endif
        V3 asteroidPos = asteroid.transform.position;
            
        // NOTE: Asteroids that are within bounds shall remain active.
        if(asteroidPos.x > minBound.x && asteroidPos.x < maxBound.x &&
           asteroidPos.y > minBound.y && asteroidPos.y < maxBound.y &&
           asteroidPos.z > minBound.z && asteroidPos.z < maxBound.z)
        {
            generator->asteroids[activeIndex++] = asteroid;
        }
    }

    // NOTE: Generating new asteroids based on the number of
    // "inactive" asteroids.
    uint32 totalInactive = MAX_ASTEROIDS - activeIndex;
#if DUMMY_ASTEROID
    uint32 asteroidsToConsiderActivating = totalInactive;
#else
    uint32 asteroidsToConsiderActivating = (uint32)RandomRange(0, totalInactive + 1);
#endif
    for(uint32 i = 0; i < asteroidsToConsiderActivating; i++)
    {
        // NOTE: Generate an asteroid.
        if(SINGLE_ASTEROID || DUMMY_ASTEROID || (RandomUnilateral() < 0.001f))
        {
            Asteroid asteroid = {};
            asteroid.transform.position = v3(RandomRange(minBound.x, maxBound.x),
                                             RandomRange(minBound.y, maxBound.y),
                                             minBound.z);
            asteroid.transform.scale = v3(RandomRange(2.0f, 8.0f));
            asteroid.transform.eulerAngles = v3(RandomRange(0.0f, 90.0f), RandomRange(0.0f, 90.0f),
                                                RandomRange(0.0f, 90.0f));
            asteroid.transform.eulerAngles = v3(0.0f);
            asteroid.rotationAxis = v3(0.0f, 1.0f, 0.0f);
            asteroid.rotationAxis = Normalize(asteroid.rotationAxis);
            asteroid.rotationSpeed = 0.0f;
            asteroid.currentRotationAngle = 0.0f;
            asteroid.direction = v3(0.0f, 0.0f, 1.0f);
            asteroid.speed = 20.0f;
            asteroid.color = Color(RandomUnilateral(), RandomUnilateral(), RandomUnilateral(), 1.0f);
            asteroid.isActive = 1;

#if SINGLE_ASTEROID
            asteroid.speed = 0.0f;
            asteroid.transform.position = playerJet.transform.position + v3(0.0f, -1.0f, -1.2f);
            asteroid.transform.eulerAngles = v3(0.0f, -22.5f, 0.0f);
            asteroid.transform.scale = v3(1.0f, 1.0f, 1.0f);
#endif
#if DUMMY_ASTEROID
            asteroid.speed = 0.0f;
            asteroid.transform.position = v3(0.0f, 0.0f, 0.0f);
            asteroid.transform.eulerAngles = v3(0.0f);
            asteroid.transform.scale = v3(2.0f, 1.0f, 1.0f);
            asteroid.color = Color(1.0f, 1.0f, 0, 1.0f);
#endif
            generator->asteroids[activeIndex++] = asteroid;
        }
    }
    
    // NOTE: Transforming vertices from local to world positions
    // (Asteroids).  TODO: Implementation for varied models. This just
    // has one asteroid 3D model for now, hence the same vertCount.
    generator->totalActive = activeIndex;
    totalActive = generator->totalActive;
    Model3D asteroidModel = game->asteroidModel;
    // TODO: This can be allocated just once at the start in permArena. MaxAsteroids * MaxVerticesPerAsteroid.
    TransformedModel *transformedModels = (TransformedModel *)PushBuffer(tempArena,
                                                                         totalActive * asteroidModel.vertCount * sizeof(TransformedModel));
    V3 *asteroidVerticesWorldPositions =
        (V3 *)PushBuffer(tempArena, totalActive * asteroidModel.vertCount * sizeof(V3));
    Asteroid *asteroids = generator->asteroids;
    uint32 stride = 0;
    for(uint32 a = 0; a < totalActive; a++)
    {
        Asteroid asteroid = asteroids[a];
        M4 asteroidModelMatrix = IdentityMatrix();
        M4 asteroidRotationMatrix = RotateAxis(asteroid.rotationAxis, asteroid.currentRotationAngle) *
            Rotate(asteroid.transform.eulerAngles);
        asteroidModelMatrix = Translate(asteroid.transform.position) * asteroidRotationMatrix *
            Scale(asteroid.transform.scale);

        uint32 vertIndex = stride;
        real32 minX = MAX_INT;
        real32 minY = MAX_INT;
        real32 minZ = MAX_INT;
        real32 maxX = MIN_INT;
        real32 maxY = MIN_INT;
        real32 maxZ = MIN_INT;

        for(int v = 0; v < asteroidModel.vertCount; v++)
        {
            V3 worldPos = asteroidModelMatrix * asteroidModel.verts[v];
            minX = (worldPos.x < minX) ? worldPos.x : minX;
            minY = (worldPos.y < minY) ? worldPos.y : minY;
            minZ = (worldPos.z < minZ) ? worldPos.z : minZ;
            maxX = (worldPos.x > maxX) ? worldPos.x : maxX;
            maxY = (worldPos.y > maxY) ? worldPos.y : maxY;
            maxZ = (worldPos.z > maxZ) ? worldPos.z : maxZ;
            
            asteroidVerticesWorldPositions[vertIndex++] = worldPos;
        }
        
        V3 min = v3(minX, minY, minZ);
        V3 max = v3(maxX, maxY, maxZ);
        V3 centre = (min + max) / 2.0f;
        Box box = { min, max, centre };
        transformedModels[a].model = game->asteroidModel;
        transformedModels[a].box = box;
        transformedModels[a].vertexWorldPositions = asteroidVerticesWorldPositions + stride;
        transformedModels[a].color = asteroid.color;
        stride += asteroidModel.vertCount;
    }

    V4 *playerJetVerticesWorldPositions;
    if(playerJet.alive)
    {    
        // NOTE: Transforming vertices from local to world positions (Player Jet).
        M4 playerJetModelMat = GetModelMatrix(playerJet.transform.position, playerJet.transform.eulerAngles,
                                              playerJet.transform.scale);
        M4 playerJetRotationMatrix = IdentityMatrix() * Rotate(playerJet.transform.eulerAngles);
        playerJetVerticesWorldPositions =
            (V4 *)PushBuffer(tempArena, fighterJetModel.vertCount * sizeof(V4));
        for(uint32 v = 0; v < fighterJetModel.vertCount; v++)
        {
            playerJetVerticesWorldPositions[v] = playerJetModelMat * fighterJetModel.verts[v];
        }

#define COLLISION 0
        
#if COLLISION
        // NOTE: Collision check.
        V3 minPosition = *(V3 *)(&playerJetVerticesWorldPositions[0]);
        V3 maxPosition = minPosition;
        for(int v = 1; v < fighterJetModel.vertCount; v++)
        {
            V3 vertexWorldPos = *(V3 *)(&playerJetVerticesWorldPositions[v]);
            minPosition.x = (vertexWorldPos.x < minPosition.x) ? vertexWorldPos.x : minPosition.x;
            minPosition.y = (vertexWorldPos.y < minPosition.y) ? vertexWorldPos.y : minPosition.y;
            minPosition.z = (vertexWorldPos.z < minPosition.z) ? vertexWorldPos.z : minPosition.z;

            maxPosition.x = (vertexWorldPos.x > maxPosition.x) ? vertexWorldPos.x : maxPosition.x;
            maxPosition.y = (vertexWorldPos.y > maxPosition.y) ? vertexWorldPos.y : maxPosition.y;
            maxPosition.z = (vertexWorldPos.z > maxPosition.z) ? vertexWorldPos.z : maxPosition.z;
        }

        // NOTE: Filtering out the asteroids for collision checking by
        // distance from the player.
        real32 playerJetAABBSize = Distance(minPosition, maxPosition);
        real32 radiusForEligibility = playerJetAABBSize * 40;
        uint32 collisionEligibleAsteroidCount = 0;
        V3 *asteroidMinMaxPositions = (V3 *)PushBuffer(tempArena, totalActive * (2 * sizeof(V3)));
        uint32 *asteroidIndices = (uint32 *)PushBuffer(tempArena, totalActive * sizeof(uint32));
        for(uint32 a = 0; a < totalActive; a++)
        {
            real32 distance = Magnitude(playerJet.transform.position - asteroids[a].transform.position);
            if(distance < radiusForEligibility)
            {
                asteroidIndices[collisionEligibleAsteroidCount] = a;
                V4 *asteroidWorldVertices =
                    &asteroidVerticesWorldPositions[(a * asteroidModel.vertCount)];
                V3 aMinPosition = *(V3 *)(&asteroidWorldVertices[0]);
                V3 aMaxPosition = aMinPosition;
                for(uint32 v = 0; v < asteroidModel.vertCount; v++)
                {
                    V3 aVWorldPos = *(V3 *)(&asteroidWorldVertices[v]);
                    aMinPosition.x = (aVWorldPos.x < aMinPosition.x) ? aVWorldPos.x : aMinPosition.x;
                    aMinPosition.y = (aVWorldPos.y < aMinPosition.y) ? aVWorldPos.y : aMinPosition.y;
                    aMinPosition.z = (aVWorldPos.z < aMinPosition.z) ? aVWorldPos.z : aMinPosition.z;

                    aMaxPosition.x = (aVWorldPos.x > aMaxPosition.x) ? aVWorldPos.x : aMaxPosition.x;
                    aMaxPosition.y = (aVWorldPos.y > aMaxPosition.y) ? aVWorldPos.y : aMaxPosition.y;
                    aMaxPosition.z = (aVWorldPos.z > aMaxPosition.z) ? aVWorldPos.z : aMaxPosition.z;
                }
                asteroidMinMaxPositions[(2 * collisionEligibleAsteroidCount)] = aMinPosition;
                asteroidMinMaxPositions[(2 * collisionEligibleAsteroidCount) + 1] = aMaxPosition;
                collisionEligibleAsteroidCount++;
            }
        }

        // NOTE: Doing AABB overlap test for the filtered-out asteroids.
        for(uint32 i = 0; i < collisionEligibleAsteroidCount; i++)
        {
            V3 asteroidMinPosition = asteroidMinMaxPositions[(2 * i)];
            V3 asteroidMaxPosition = asteroidMinMaxPositions[(2 * i) + 1];

            if(AreAABBOverlapping(minPosition, maxPosition,
                                  asteroidMinPosition, asteroidMaxPosition))
            {
                playerJet.color.g = (uint32)Clamp(0, 255.0f, playerJet.color.g - 50);
                playerJet.color.b = (uint32)Clamp(0, 255.0f, playerJet.color.b - 50);
                playerJet.moveSpeed = 0.0f;
                playerJet.alive = 0;
                InitializeParticleEmitter(&game->explosionEmitter, -playerJet.forward);
            }
        }
#endif
    }
    
    Color playerJetColor = playerJet.color;
    
    // NOTE: Camera look-at matrix calculation.    
    camera.transform.position = playerJet.transform.position
        + (-5.0f * playerJet.forward)
        + (1.0f * playerJet.up);
    camera.transform.eulerAngles.y = playerJet.transform.eulerAngles.y;

#if DUMMY_ASTEROID
    camera.transform.position = v3(0.0f);
    camera.transform.eulerAngles = v3(0.0f);
#endif

    V3 rotAxisY = { 0.0f, 1.0f, 0.0f };
    V3 front = { 0.0f, 0.0f, 1.0f };
    V3 left = { 1.0f, 0.0f, 0.0f };
    camera.front = RotatePoint(front, rotAxisY, camera.transform.eulerAngles.y);
    camera.left = RotatePoint(left, rotAxisY, camera.transform.eulerAngles.y);
    camera.front = RotatePoint(camera.front, camera.left, camera.transform.eulerAngles.x);
    M4 v = LookAt(camera.transform.position, camera.transform.position - camera.front,
                  camera.front, camera.left);
#if 0
    M4 p = Orthographic(camera.nPlane, camera.fPlane, aspectRatio, 10.0f);
#else
    M4 p = Perspective(camera.nPlane, camera.fPlane, aspectRatio, camera.fov);
#endif
    M4 pv = p * v;

    // NOTE: Explosion simulation.    
    if(game->explosionEmitter.enabled)
    {
        SimulateParticles(&game->explosionEmitter, deltaTime);
    }

#define OPTIMIZED_RENDERING 1
    
    // NOTE: Rendering.
    Color black = Color(0.0f, 1.0f);
    Color blue = Color(0.0f, 0.0f, 1.0f, 1.0f);
    ClearBuffer(zBuffer, 1.0f, totalPixels);
    ClearBuffer(colorBuffers[0], blue);
    ClearBuffer(colorBuffers[1], black);


#if !DUMMY_ASTEROID
    if(playerJet.alive)
    {
        /*
        RenderModel(fighterJetModel, renderBuffer, pv, playerJetVerticesWorldPositions,
                    tempArena, game->light, camera.transform.position, 1, 0);
        */
    }
#endif

    RenderModelsOptimized_8x(transformedModels, totalActive, pv, tempArena, bufferWidth, bufferHeight,
                           colorBuffers[0], zBuffer);
    //RenderDot(colorBuffers[0], 1005.5f, 118.5f, 1.0f, Color(1.0f, 1.0f, 1.0f, 1.0f));

#if 0
    Model3D quadModel;
    CopyBuffer(&game->quadModel, &quadModel, sizeof(game->quadModel));
    quadModel.colorTex = &game->explosionParticleTex;

    uint32 totalParticles = game->explosionEmitter.maxParticles;
    Particle *particles = game->explosionEmitter.particles;
    for(uint32 i = 0; i < totalParticles; i++)
    {
        if(particles[i].isActive)
        {
            Transform t = particles[i].transform;
            M4 modelMatrix = LookAt(t.position, camera.transform.position, t.scale);
            RenderModel(quadModel, renderBuffer, pv, modelMatrix,
                        tempArena, game->light, camera.transform.position, 0, 1);
        }
    }
#endif

#if USER_INTERFACE
    RenderUIElements(game->uiElements, GameScreenUI_Count,
                     renderBuffer.width, renderBuffer.height, renderBuffer.pixels);
#endif
    //PrintValueUInt32((uint32)deltaTimeMs, 1700, 1000, Color(255), game->glyphs, GLYPH_START_ASCII, GLYPH_COUNT,
    //                renderBuffer, &game->tempArena);
    // NOTE: Updating game structure with changes.
    game->camera = camera;
    game->playerJet = playerJet;


    uint32 *destPixels = pixels;
    real32 *reds = game->colorBuffers[0].rs;
    real32 *greens = game->colorBuffers[0].gs;
    real32 *blues = game->colorBuffers[0].bs;
    real32 *alphas = game->colorBuffers[0].as;
    
    __m128 _One = _mm_set_ps(1.0f, 1.0f, 1.0f, 1.0f);
    __m128 _Zero = _mm_set_ps(0.0f, 0.0f, 0.0f, 0.0f);
    __m128 _Two55 = _mm_set_ps(255.0f, 255.0f, 255.0f, 255.0f);

    __m256 _One_8x = _mm256_set1_ps(1.0f);
    __m256 _Zero_8x = _mm256_set1_ps(0.0f);
    __m256 _Two55_8x = _mm256_set1_ps(255.0f);
    uint32 totalSets = totalPixels / 4;
    uint32 totalSets_8x = totalPixels / 8;

#define AVX 1
#define SSE 0
#if AVX
    for(uint32 i = 0; i < totalSets_8x; i++)
    {
        //Color *colors = srcPixels;        
        __m256 _Rs = _mm256_set_ps(*reds++, *reds++, *reds++, *reds++,
                                   *reds++, *reds++, *reds++, *reds++);

        __m256 _Gs = _mm256_set_ps(*greens++, *greens++, *greens++, *greens++,
                                   *greens++, *greens++, *greens++, *greens++);

        __m256 _Bs = _mm256_set_ps(*blues++, *blues++, *blues++, *blues++,
                                   *blues++, *blues++, *blues++, *blues++);

        __m256 _As = _mm256_set_ps(*alphas++, *alphas++, *alphas++, *alphas++,
                                   *alphas++, *alphas++, *alphas++, *alphas++);

        _Rs = _mm256_min_ps(_mm256_max_ps(_Rs, _Zero_8x), _One_8x);
        _Rs = _mm256_mul_ps(_Rs, _Two55_8x);

        _Gs = _mm256_min_ps(_mm256_max_ps(_Gs, _Zero_8x), _One_8x);
        _Gs = _mm256_mul_ps(_Gs, _Two55_8x);

        _Bs = _mm256_min_ps(_mm256_max_ps(_Bs, _Zero_8x), _One_8x);
        _Bs = _mm256_mul_ps(_Bs, _Two55_8x);

        _As = _mm256_min_ps(_mm256_max_ps(_As, _Zero_8x), _One_8x);
        _As = _mm256_mul_ps(_As, _Two55_8x);

        __m256i _Rsi = _mm256_cvttps_epi32(_Rs);
        __m256i _Gsi = _mm256_cvttps_epi32(_Gs);
        __m256i _Bsi = _mm256_cvttps_epi32(_Bs);
        __m256i _Asi = _mm256_cvttps_epi32(_As);

        __m256i _RGi = _mm256_or_si256(_mm256_slli_epi32(_Rsi, 16), _mm256_slli_epi32(_Gsi, 8));
        __m256i _BAi = _mm256_or_si256(_Bsi, _mm256_slli_epi32(_Asi, 24));
        __m256i _RGBA = _mm256_or_si256(_RGi, _BAi);

        *(__m256i *)destPixels = _RGBA;

        destPixels += 8;
    }
#elif SSE
    for(uint32 i = 0; i < totalSets; i++)
    {
        Color *colors = srcPixels;
        
        __m128 rs = _mm_set_ps(colors[0].r, colors[1].r, colors[2].r, colors[3].r);
        __m128 gs = _mm_set_ps(colors[0].g, colors[1].g, colors[2].g, colors[3].g);
        __m128 bs = _mm_set_ps(colors[0].b, colors[1].b, colors[2].b, colors[3].b);
        __m128 as = _mm_set_ps(colors[0].a, colors[1].a, colors[2].a, colors[3].a);

        rs = _mm_min_ps(rs, _One);
        rs = _mm_max_ps(rs, _Zero);
        rs = _mm_mul_ps(rs, _Two55);

        gs = _mm_min_ps(gs, _One);
        gs = _mm_max_ps(gs, _Zero);
        gs = _mm_mul_ps(gs, _Two55);

        bs = _mm_min_ps(bs, _One);
        bs = _mm_max_ps(bs, _Zero);
        bs = _mm_mul_ps(bs, _Two55);

        as = _mm_min_ps(as, _One);
        as = _mm_max_ps(as, _Zero);
        as = _mm_mul_ps(as, _Two55);

        __m128i _Rs = _mm_cvtps_epi32(rs);
        __m128i _Gs = _mm_cvtps_epi32(gs);
        __m128i _Bs = _mm_cvtps_epi32(bs);
        __m128i _As = _mm_cvtps_epi32(as);

        uint32 *reds = (uint32 *)&_Rs;
        uint32 *greens = (uint32 *)&_Gs;
        uint32 *blues = (uint32 *)&_Bs;
        uint32 *alphas = (uint32 *)&_As;

        for(uint32 j = 0; j < 4; j++)
        {
            *destPixels++ = (alphas[j]<<24) | (reds[j]<<16) | (greens[j]<<8) | blues[j];
        }

        srcPixels += 4;
    }
#else
    for(uint32 i = 0; i < totalPixels; i++)
    {
        Color color = *srcPixels++;
        uint32 r = Clamp(color.r, 0.0f, 1.0f) * 255.0f;
        uint32 g = Clamp(color.g, 0.0f, 1.0f) * 255.0f;
        uint32 b = Clamp(color.b, 0.0f, 1.0f) * 255.0f;
        uint32 a = Clamp(color.a, 0.0f, 1.0f) * 255.0f;
        *destPixels++ = (a<<24) | (r<<16) | (g<<8) | a;
    }
#endif
}

extern "C" void
GameUpdateAndRender(GameData gameData)
{ 
#if ZIRANG_DEBUG
    debugGameMemory = gameData.memory;
#endif
    
    START_CYCLE_TICK(GAME_UPDATE_AND_RENDER);
        
    GameBuffer *buffer = gameData.buffer;
    GameMemory *memory = gameData.memory;
    GameTiming timing = gameData.timing;
    GameInput *input = gameData.input;
    PlatformFunctions platform = gameData.platform;

    Game *game;
    if(memory->permanentMemory)
    {
        game = (Game *)memory->permanentMemory;
    }
    else
    {
        memory->permanentMemorySize = 500 * MB;
        memory->transientMemorySize = 500 * MB;
        uint32 totalBytes = memory->permanentMemorySize + memory->transientMemorySize;
        if(platform.PlatformGetMemory)
        {
            memory->permanentMemory = (uint32 *)platform.PlatformGetMemory(totalBytes);
            memory->transientMemory = (uint8 *)memory->permanentMemory +
                memory->permanentMemorySize;
        }
        
        if(!memory->permanentMemory || !memory->transientMemory)
        {
            Assert(0);
        }
        else
        {
            // NOTE: This is the initialization code.
            game = (Game *)memory->permanentMemory;
            game->permArena.isCircular = 0;
            game->permArena.base = memory->permanentMemory;
            game->permArena.size = memory->permanentMemorySize;
            game->permArena.end = (void *)(((char *)game->permArena.base) +
                                           game->permArena.size - 1);
            game->permArena.current = game->permArena.base;
            game->permArena.used = 0;
            game = (Game *)PushBuffer(&game->permArena, sizeof(Game));

            uint32 width = buffer->width;
            uint32 height = buffer->height;
            uint32 bytesPerPixel = buffer->bytesPerPixel;
            uint32 totalPixels = width * height;
            uint32 totalBytes = totalPixels * bytesPerPixel;
            game->zBuffer = (real32 *)PushBuffer(&game->permArena, totalBytes);
            //game->colorBuffer0 = (Color *)PushBuffer(&game->permArena, totalPixels * sizeof(Color));
            //game->colorBuffer1 = (Color *)PushBuffer(&game->permArena, totalPixels * sizeof(Color));

            uint32 channelCount = 4;
            uint32 bufferCount = 2;
            real32 *channels = (real32*)PushBuffer(&game->permArena,
                                                   bufferCount * channelCount * totalPixels * sizeof(real32));
            for(uint32 i = 0; i < bufferCount; i++)
            {
                game->colorBuffers[i].width = width;
                game->colorBuffers[i].height = height;
                
                game->colorBuffers[i].rs = channels;
                channels += totalPixels;
                game->colorBuffers[i].gs = channels;
                channels += totalPixels;
                game->colorBuffers[i].bs = channels;
                channels += totalPixels;
                game->colorBuffers[i].as = channels;
                channels += totalPixels;
            }

            if(!buffer->pixels)
            {
                buffer->pixels = (uint32 *)PushBuffer(&game->permArena, totalBytes);
            }

            game->tempArena.isCircular = 1;
            game->tempArena.base = memory->transientMemory;
            game->tempArena.size = memory->transientMemorySize;
            game->tempArena.end = (void *)(((char *)game->tempArena.base) +
                                           game->tempArena.size - 1);
            game->tempArena.current = game->tempArena.base;
            game->tempArena.used = 0;

            if(platform.PlatformReadFile)
            {
                PlatformReadFileResult forr =
                    ReadFile("..//Zirang//Data//fighter.obj", &game->tempArena, platform);
                PlatformReadFileResult fmrr =
                    ReadFile("..//Zirang//Data//fighter.mtl", &game->tempArena, platform);
                PlatformReadFileResult testBmp =
                    ReadFile("..//Zirang//Data//test.bmp", &game->tempArena, platform);
                PlatformReadFileResult eprr =
                    ReadFile("..//Zirang//Data//transparent.bmp", &game->tempArena, platform);
                PlatformReadFileResult attf =
                    ReadFile("..//Zirang//Data//arial.ttf", &game->tempArena, platform);

                stbtt_fontinfo fontInfo = {};
                int fontResult = stbtt_InitFont(&fontInfo, (uint8*)attf.data, 0);

                real32 fontSize = 32.0f;
                real32 fontScale = fontSize / 1491.0f;

                for(uint32 i = 0; i < GLYPH_COUNT; i++)
                {
                    uint8 charAscii = (uint8)(GLYPH_START_ASCII + i);
                    int width, height;
                    int xOff, yOff;                
                    uint8 *fontCodepointData = stbtt_GetCodepointBitmap(&fontInfo, fontScale, fontScale, charAscii,
                                                                        &width, &height, &xOff, &yOff);
                    game->glyphs[i] = GetLoadedBitmapFromFont(fontCodepointData, width, height, &game->permArena);
                    stbtt_FreeBitmap(fontCodepointData, attf.data);
                }
                
                if(forr.success && fmrr.success)
                {
                    GetObjMeshData(forr.size, forr.data,
                                   fmrr.size, fmrr.data,
                                   &game->fighterJetModel, &game->permArena);
                }
                else
                {
                    Assert(0);
                }

                //game->colorTex = GenerateColorTex(&game->permArena, 256, 256, 32);
                //game->testBitmap = GetLoadedBitmap(testBmp.size, testBmp.data, &game->permArena);
                game->explosionParticleTex = GetLoadedBitmap(eprr.size, eprr.data, &game->permArena);

                // Quad model
                V3 *quadVerts = (V3 *)PushBuffer(&game->permArena, 4 * sizeof(V3));
                uint32 *quadIndices = (uint32 *)PushBuffer(&game->permArena, 6 * sizeof(uint32));
                V2 *quadUVs = (V2 *)PushBuffer(&game->permArena, 6 * sizeof(V2));
                
                quadVerts[0] = v3(-0.5f, -0.5f,  0.0f); // front-bottom-left
                quadVerts[1] = v3(-0.5f,  0.5f,  0.0f); // front-top-left
                quadVerts[2] = v3( 0.5f,  0.5f,  0.0f); // front-top-right
                quadVerts[3] = v3( 0.5f, -0.5f,  0.0f); // front-bottom-right
                quadIndices[0] = 3;
                quadIndices[1] = 1;
                quadIndices[2] = 0;
                quadIndices[3] = 1;
                quadIndices[4] = 3;
                quadIndices[5] = 2;
                quadUVs[0] = v2(1.0f, 0.0f);
                quadUVs[1] = v2(0.0f, 1.0f);
                quadUVs[2] = v2(0.0f, 0.0f);
                quadUVs[3] = v2(0.0f, 1.0f);
                quadUVs[4] = v2(1.0f, 0.0f);
                quadUVs[5] = v2(1.0f, 1.0f);

                game->quadModel = { 4, 2, quadVerts, quadIndices, quadUVs,
                                    0 };

                // Cube model.
                V3 cubeVerts[8];
                cubeVerts[0] = v3(-0.5f, -0.5f,  0.5f); // front-bottom-left
                cubeVerts[1] = v3(-0.5f,  0.5f,  0.5f); // front-top-left
                cubeVerts[2] = v3( 0.5f,  0.5f,  0.5f); // front-top-right
                cubeVerts[3] = v3( 0.5f, -0.5f,  0.5f); // front-bottom-right
                cubeVerts[4] = v3( 0.5f, -0.5f, -0.5f); // behind-bottom-right
                cubeVerts[5] = v3( 0.5f,  0.5f, -0.5f); // behind-top-right
                cubeVerts[6] = v3(-0.5f,  0.5f, -0.5f); // behind-top-left
                cubeVerts[7] = v3(-0.5f, -0.5f, -0.5f); // behind-bottom-left

                uint32 faceCount = 6;
                uint32 triCount = 2*faceCount;
                uint32 totalIndices = 3*triCount;

                uint32 indices[36] = { 3, 1, 0,    1, 3, 2, // Front
                                       7, 5, 4,    5, 7, 6, // Back
                                       4, 2, 3,    2, 4, 5, // Right
                                       0, 6, 7,    6, 0, 1, // Left
                                       2, 6, 1,    6, 2, 5, // Top
                                       4, 0, 7,    0, 4, 3 }; // Bottom
                
                V2 *cubeUVs = (V2 *)PushBuffer(&game->tempArena, totalIndices * sizeof(V2));
                for(uint32 i = 0; i < totalIndices; i++)
                {
                    *(cubeUVs+i) = quadUVs[i % 6];
                }

                // SUBDIVIDE CUBE
                uint32 subdivideLevels = 0;
                uint32 currentTriCount = ARRAY_COUNT(indices, uint32) / 3;
                uint32 newTriCount = currentTriCount * pow(2, subdivideLevels);
                uint32 newVertCount = ARRAY_COUNT(cubeVerts, V3) + (currentTriCount * (pow(2, subdivideLevels) - 1));
                uint32 newIndicesCount = newTriCount * 3;

                V3 *exCubeVerts = (V3 *)PushBuffer(&game->permArena, newVertCount * sizeof(V3));
                uint32 *exCubeIndices = (uint32 *)PushBuffer(&game->permArena, newIndicesCount * sizeof(uint32));
                V2 *exCubeUVs = (V2 *)PushBuffer(&game->permArena, newIndicesCount * sizeof(V2));

                CopyBuffer(indices, exCubeIndices, sizeof(indices));
                CopyBuffer(cubeVerts, exCubeVerts, sizeof(cubeVerts));
                CopyBuffer(cubeUVs, exCubeUVs, totalIndices * sizeof(V2));

                uint32 currentVertCount = ARRAY_COUNT(cubeVerts, V3);
                for(uint32 l = 0; l < subdivideLevels; l++)
                {
                    for(int t = 0; t < currentTriCount; t++)
                    {
                        uint32 triIndex = 3 * t;
                        uint32 i0 = exCubeIndices[triIndex];
                        uint32 i1 = exCubeIndices[triIndex+1];
                        uint32 i2 = exCubeIndices[triIndex+2];
                        V3 v0 = exCubeVerts[i0];
                        V3 v1 = exCubeVerts[i1];
                        V3 v2 = exCubeVerts[i2];
                        V2 uv0 = exCubeUVs[triIndex];
                        V2 uv1 = exCubeUVs[triIndex+1];
                        V2 uv2 = exCubeUVs[triIndex+2];

                        V3 v3 = (v1 + v2) / 2.0f;
                        V2 uv3 = (uv1 + uv2) / 2.0f;
                        uint32 currentTriIndex = currentTriCount + t;
                        uint32 currentIndexIndex = currentTriIndex * 3;

                        exCubeVerts[currentVertCount] = v3;
                        exCubeIndices[triIndex] = i0;
                        exCubeIndices[triIndex+1] = i1;
                        exCubeIndices[triIndex+2] = currentVertCount;
                        exCubeUVs[triIndex] = uv0;
                        exCubeUVs[triIndex+1] = uv1;
                        exCubeUVs[triIndex+2] = uv3;

                        exCubeIndices[currentIndexIndex+0] = i0;
                        exCubeIndices[currentIndexIndex+1] = currentVertCount;
                        exCubeIndices[currentIndexIndex+2] = i2;
                        exCubeUVs[currentIndexIndex+0] = uv0;
                        exCubeUVs[currentIndexIndex+1] = uv3;
                        exCubeUVs[currentIndexIndex+2] = uv2;

                        currentVertCount++;
                    }
                    currentTriCount *= 2;
                }
                
                Model3D asteroidModel3D = { currentVertCount, currentTriCount, exCubeVerts, exCubeIndices, exCubeUVs,
                                            0 };
                game->asteroidModel = asteroidModel3D;

            }
            
            game->camera.transform.position = v3(0.0f, 0.0f, 10.0f);
            game->camera.transform.eulerAngles = v3(0);
            game->camera.fov = 60;
            game->camera.nPlane = 1.0f;
            game->camera.fPlane = 10000.0f;

            game->playerJet.alive = 1;
            game->playerJet.transform.position = v3(0.0f);
            game->playerJet.transform.eulerAngles = v3(0.0f);
            game->playerJet.transform.scale = v3(0.25f);
            game->playerJet.mass = 5000;
            game->playerJet.thrustWeightRatio = 1.5f;
            game->playerJet.gravityForce = v3(0.0f, -game->playerJet.mass * 9.8f, 0.0f);
            game->playerJet.liftForce = -game->playerJet.gravityForce;
            real32 thrustForceMagnitude = Magnitude(game->playerJet.gravityForce) *
                game->playerJet.thrustWeightRatio;
            game->playerJet.thrustForce = v3(0.0f, 0.0f, -thrustForceMagnitude);
            game->playerJet.dragForce = -game->playerJet.thrustForce;
#if DUMMY_ASTEROID || SINGLE_ASTEROID
            game->playerJet.moveSpeed = 0.0f;
#else
            game->playerJet.moveSpeed = 50.0f;
#endif
            game->playerJet.maneuverMultiplier = game->playerJet.moveSpeed / 10.0f;
            game->playerJet.velocity = v3(0.0f, 0.0f, -game->playerJet.moveSpeed);
            game->playerJet.tiltSpeed = 90.0f;
            game->playerJet.tiltMax = 60.0f;
            game->playerJet.tiltAngle = 0.0f;
            game->playerJet.tiltDampener = 1.5f;
            game->playerJet.forward = v3(0.0f, 0.0f, -1.0f);
            game->playerJet.right = v3(1.0f, 0.0f, 0.0f);
            game->playerJet.up = v3(0.0f, 1.0f, 0.0f);
            game->playerJet.color = Color(255);

            AsteroidGenerator *generator = &game->asteroidGenerator;
            generator->boundSize = 1000.0f;
            generator->totalActive = 0;

#if PARTICLE_SYSTEM
            uint32 particleEmitterCapacity = 4;
            ParticleEmitter emitter = {};
            emitter.enabled = 0;
            emitter.transform = Transform();
            emitter.maxParticles = particleEmitterCapacity;
            emitter.lifetime = 5.0f;
            emitter.maxSpeed = 1.0f;
            emitter.albedo = Color(1.0f);
            emitter.tex = &game->explosionParticleTex;
            emitter.particles = (Particle *)PushBuffer(&game->permArena, particleEmitterCapacity * sizeof(Particle));
            game->explosionEmitter = emitter;
#endif

            UIElement *uiElements = game->uiElements;
            UIElement steerLeftButton = { UIElement_Button, 200, 200, 200, 100,
                                          Color(255), Color(128), OnSteerLeft, 0 };
            UIElement steerRightButton = { UIElement_Button, 800, 200, 200, 100,
                                           Color(255), Color(128), OnSteerRight, 0 };

            uiElements[GameScreenUI_SteerLeftButton] = steerLeftButton;
            uiElements[GameScreenUI_SteerRightButton] = steerRightButton;

            Screen mainMenuScreen = { ScreenType_MainMenu, MainMenuScreenUpdate };
            Screen gameScreen = { ScreenType_Game, GameScreenUpdate };
            game->screens[ScreenType_MainMenu] = mainMenuScreen;
            game->screens[ScreenType_Game] = gameScreen;
            game->Update = GameScreenUpdate;
        }
    }

    game->Update(gameData);
    END_CYCLE_TICK(GAME_UPDATE_AND_RENDER);
}
