#if !defined(ZIRANG_FONT_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

LoadedBitmap
GetLoadedBitmapFromFont(uint8 *glyphData, uint32 width, uint32 height, MemoryArena *permArena)
{
    LoadedBitmap result = {};
    result.width = width;
    result.height = height;
    result.bitsPerPixel = 32;
    
    uint32 totalPixels = width * height;
    result.data = (Color *)PushBuffer(permArena, totalPixels * sizeof(Color));

    Color *pixelRow = result.data;
    uint8 *glyphRow = glyphData + ((height-1) * width);
    
    for(uint32 r = 0; r < height; r++)
    {
        uint8 *glyphPoint = glyphRow;
        Color *pixelPoint = pixelRow;
        for(uint32 c = 0; c < width; c++)
        {
            real32 glyphValue = (*glyphPoint) / 255.0f;
            Color color = Color(glyphValue, glyphValue, glyphValue, glyphValue);
            *pixelPoint = color;
            pixelPoint++;
            glyphPoint++;
        }
        
        pixelRow += width;
        glyphRow -= width;
    }

    return result;
}


#define ZIRANG_FONT_H
#endif
