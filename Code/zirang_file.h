#if !defined(ZIRANG_FILE_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

//#include "zirang_string.h"

struct FileWordsData
{
    uint32 count;
    void **ptrs;
    uint32 *lengths;
};

FileWordsData
GetFileWords(uint32 size, void *data, MemoryArena *arena)
{
    FileWordsData result;
    char *chars = (char *)data;
    uint32 wordCount = 0;
    bool32 foundSeparator = 1;

    for(int c = 0; c < size; c++)
    {
        if(chars[c] == ' ' || chars[c] == '\n')
        {
            foundSeparator = 1;
        }
        else // found a word-character.
        {
            if(foundSeparator) // This is the first character after a separator.
            {
                wordCount++;
            }
            foundSeparator = 0;
        }
    }

    void **wordPtrs = (void **)PushBuffer(arena, wordCount * sizeof(void *));
    uint32 *wordLength = (uint32 *)PushBuffer(arena, wordCount * sizeof(uint32 *));

    void **currWordPtr = wordPtrs;
    uint32 wordIndex = 0;
    uint32 currWordLength = 0;
    foundSeparator = 1;
    for(int c = 0; c < size; c++)
    {
        if(chars[c] == ' ' || chars[c] == '\n')
        {
            if(!foundSeparator)
            {
                wordIndex++;
            }

            currWordLength = 0;
            foundSeparator = 1;
        }
        else // found a word-character.
        {
            if(foundSeparator) // This is the first character after a separator.
            {
                currWordPtr = wordPtrs + wordIndex;
                *currWordPtr = &chars[c];
            }

            foundSeparator = 0;
            currWordLength++;
            wordLength[wordIndex] = currWordLength;
        }
    }

    result.count = wordCount;
    result.ptrs = wordPtrs;
    result.lengths = wordLength;

    return result;
}

PlatformReadFileResult
ReadFile(char *filename, MemoryArena *arena, PlatformFunctions platform)
{
    PlatformReadFileResult result;
    uint32 fileSize = platform.PlatformGetFileSize(filename);
    if(fileSize != 0)
    {
        void *data = PushBuffer((MemoryArena*)arena, fileSize);
        platform.PlatformReadFile(filename, &result, data);
    }
    return result;
}

#define ZIRANG_FILE_H
#endif
