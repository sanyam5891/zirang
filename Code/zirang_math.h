#if !defined(ZIRANG_MATH_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#define ZIRANG_MATH_H

// TODO: Remove this later.
#include <cmath>
#define PI 3.14159

// TODO: When inlining was on, it was returning non-zero
// (e^-highvalue) instead of exact 0 (when "value" was 0). This
// resulted in major logical flaws. Look into the disassembly
// generated.
// TODO: Optimize.
inline real32
Abs(real32 value)
{
    real32 result = value;
    if(result < 0.0f)
        result *= (-1.0f);
    return result;
}

// TODO: Optimize.
inline uint8
Clamp(uint8 value, uint8 low, uint8 high)
{
    uint8 result = (value < low) ? low : ((value > high) ? high : value);
    return result;
}

// TODO: Optimize.
inline uint32
Clamp(uint32 value, uint32 low, uint32 high)
{
    uint32 result = (value < low) ? low : ((value > high) ? high : value);
    return result;
}

// TODO: Optimize.
inline real32
Clamp(real32 value, real32 low, real32 high)
{    
    real32 result = (value < low) ? low : ((value > high) ? high : value);
    return result;
}

// TODO: Optimize.
inline real32
Floor(real32 value)
{
    real32 result = (uint32)((value < 0) ? value-1 : value);
    return result;
}

// TODO: Optimize.
inline real32
Ceil(real32 value)
{
    real32 result = (uint32)((value < 0) ? value : value+1);
    return result;
}

// TODO: Optimize.
inline real32
Wrap(real32 low, real32 high, real32 val)
{
    real32 result = (val < low) ? high : ((val > high) ? low : val);
    return result;
}

// TODO: Optimize.
inline int32
Roundf(real32 value)
{
    int32 result = value + ((value < 0.0f) ? -0.5f : 0.5f);
    return result;
}

// TODO: Optimize.
inline int32
RoundToNearestMultiple(real32 value, int32 multiple)
{
    int32 absMultiple = (int32)Abs((real32)multiple);
    real32 quotient = value / (real32)absMultiple;
    int32 quotientInt = Roundf(quotient);
    int32 result = quotientInt * absMultiple;
    return result;
}

// TODO: Optimize.
inline real32
Lerp(real32 start, real32 end, real32 proportion)
{
    real32 result = start + ((end - start)*proportion);
    return result;
}

// TODO: Optimize.
inline real32
GetProportion(real32 start, real32 end, real32 value)
{
    real32 result = (value - start) / (end - start);
    return result;
}

inline int32
GetFirstSetBitPosition(uint32 value)
{
    int32 result = -1;
    uint32 mask = 1;
    for(int i = 0; i < 32; i++)
    {
        if((value & mask) > 0)
        {
            result = i;
            break;
        }
        mask = mask << 1;
    }
    return result;
}

// TODO: Optimize.
inline real32
RandomUnilateral()
{
    real32 result = (real32)rand() / (real32)RAND_MAX;
    return result;
}

// TODO: Optimize.
inline real32
RandomBilateral()
{
    real32 result = (2.0f * RandomUnilateral()) - 1.0f;
    return result;
}

// TODO: Optimize.
real32
RandomRange(real32 min, real32 max)
{
    real32 result = Lerp(min, max, RandomUnilateral());
    return result;
}

#endif
