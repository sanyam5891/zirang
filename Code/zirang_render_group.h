#if !defined(ZIRANG_RENDER_GROUP_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#define ZIRANG_RENDER_GROUP_H

#include "zirang_color.h"
#include "zirang_font.h"

struct IntegerToStringResult
{
    uint32 digitCount;
    uint8 *digitsAscii;
};

inline real32
EdgeTest(V2 point, V2 v0, V2 n01)
{
    V2 v0p = point - v0;
    real32 result = Dot(n01, v0p);
    return result;
}

inline bool32
IsPointInsideTriangle(V2 point, V2 v0, V2 v1, V2 v2,
              V2 n01, V2 n12, V2 n20)
{
    real32 e0 = EdgeTest(point, v0, n01);
    real32 e1 = EdgeTest(point, v1, n12);
    real32 e2 = EdgeTest(point, v2, n20);

#if 0 // Clockwise.
    if((e0 >= 0 && e1 >= 0 && e2 >= 0))
    {
        return 1;
    }
#endif

#if 1 // Anti-clockwise
    if((e0 <= 0 && e1 <= 0 && e2 <= 0))
    {
        return 1;
    }
#endif

    return 0;
}

inline bool32
IsTriangleClockwise(V2 v0, V2 v1, V2 v2,
                    V2 n01, V2 n12, V2 n20)
{
    V2 centre = (v0 + v1 + v2) / 3.0f;
    bool32 result = IsPointInsideTriangle(centre, v0, v1, v2, n01, n12, n20);
    return result;
}

inline V3
GetTriangleNormal(V3 v0, V3 v1, V3 v2)
{
    V3 axis10 = Normalize(v1 - v0);
    V3 axis20 = Normalize(v2 - v0);
    V3 normal = Normalize(Cross(axis10, axis20));
    return normal;
}

bool8
ZTest(void *zBuffer, uint32 bWidth, V3 point)
{
    real32 *z = (real32 *)zBuffer + ((uint32)point.y * bWidth) + (uint32)point.x;
    real32 prevZVal = *z;
    real32 currZVal = point.z;

    if(currZVal < prevZVal)
    {
        //*z = currZVal;
        return 1;
    }

    return 0;
    
}

void
ApplyZ(void *zBuffer, uint32 bWidth, V2 point, real32 val)
{
    real32 *z = (real32 *)zBuffer + ((uint32)point.y * bWidth) + (uint32)point.x;
    *z = val;
}

real32
DistancePointFromLine(V2 lineP0, V2 lineP1, V2 point)
{
    V2 lineDir = lineP0 - lineP1;
    V2 lineDirNormal = Normalize(lineDir);
    V2 pointDir = point - lineP1;
    V2 pointDirNormal = Normalize(pointDir);
    real32 dot = Dot(lineDirNormal, pointDirNormal);
    real32 angleRad = acos(dot);
    real32 result = Magnitude(pointDir) * sinf(angleRad);
    return result;
}

Color
GetBitmapTexel(LoadedBitmap bitmap, real32 u, real32 v)
{
    Color *colors = bitmap.data;
    real32 x = u * bitmap.width - 0.5f;
    real32 y = v * bitmap.height - 0.5f;
    uint32 xf = (uint32)x;
    uint32 yf = (uint32)y;

#if 1
    uint32 xc = xf+1;
    uint32 yc = yf+1;

    if(x < 0.0f)
        xc = xf;
    else if(xc >= bitmap.width)
        xc = xf;

    if(y < 0.0f)
        yc = yf;
    else if(yc >= bitmap.height)
        yc = yf;

    real32 xProportion = 1.0f - (x - xf);
    real32 yProportion = 1.0f - (y - yf);

    Color bl = *(colors + (yf * bitmap.width) + xf);
    Color br = *(colors + (yf * bitmap.width) + xc);
    Color tl = *(colors + (yc * bitmap.width) + xf);
    Color tr = *(colors + (yc * bitmap.width) + xc);

    Color bcs = LerpColor(bl, br, xProportion);
    Color tcs = LerpColor(tl, tr, xProportion);
    Color result = LerpColor(bcs, tcs, yProportion);
#else
    Color result = *(colors + (yf * bitmap.width) + xf);
#endif
    return result;
}

void
Blur(uint32 *src, uint32 *dst, uint32 width, uint32 height, bool8 horizontalBlur)
{
    //real32 weights[5] = {0.227027f, 0.1945946f, 0.1216216f, 0.054054f, 0.016216f};
    real32 weights[3] = { 0.1f, 0.45f };
    real32 size = ARRAY_COUNT(weights, real32);

    uint32 *rowSrc = src;
    uint32 *rowDst = dst;

    if(horizontalBlur)
    {
        for(uint32 row = 0; row < height; row++)
        {
            uint32 *currentSrc = rowSrc;
            uint32 *currentDst = rowDst;
        
            for(uint32 col = 0; col < width; col++)
            {
                Color src0c = weights[0] * U32ToColor(*currentSrc);
                Color destc = src0c;

                for(uint32 k = 1; k < size; k++)
                {
                    if((col-k) >= 0)
                    {
                        Color srcklc = weights[k] * U32ToColor(*(currentSrc-k));
                        destc = destc + srcklc;
                    }

                    if((col+k) < width)
                    {
                        Color srckrc = weights[k] * U32ToColor(*(currentSrc+k));
                        destc = destc + srckrc;
                    }
                }

                *currentDst = (uint32)destc;

                currentSrc++;
                currentDst++;
            }

            rowSrc += width;
            rowDst += width;
        }
    }
    else
    {
        for(uint32 row = 0; row < height; row++)
        {
            uint32 *currentSrc = rowSrc;
            uint32 *currentDst = rowDst;
        
            for(uint32 col = 0; col < width; col++)
            {
                Color src0c = weights[0] * U32ToColor(*currentSrc);
                Color destc = src0c;

                for(uint32 k = 1; k < size; k++)
                {
                    if((row-k) >= 0)
                    {
                        Color srckdc = weights[k] * U32ToColor(*(currentSrc-(k*width)));
                        destc = destc + srckdc;
                    }

                    if((row+k) < width)
                    {
                        Color srckuc = weights[k] * U32ToColor(*(currentSrc+(k*width)));
                        destc = destc + srckuc;
                    }
                }

                *currentDst = (uint32)destc;

                currentSrc++;
                currentDst++;
            }

            rowSrc += width;
            rowDst += width;
        }        
    }
}

struct TriangleRenderData
{
    V3 worldPositions[3];
    V3 screenPoints[3];
    Color color;
};

bool8
IsBoxVisible(V3 min, V3 max, M4 projectionView, uint32 bufferWidth, uint32 bufferHeight)
{
    bool8 result = 0;
    V3 vertices[8] = { min, v3(max.x, min.y, min.z), v3(min.x, min.y, max.z), v3(max.x, min.y, max.z),
                       max, v3(max.x, max.y, min.z), v3(min.x, max.y, max.z), v3(min.x, max.y, min.z) };
    for(uint32 i = 0; i < 8; i++)
    {
        V4 preClippedPoint = projectionView * vertices[i];
        ScreenPointResult pointResult = GetScreenCoordinates(preClippedPoint, bufferWidth, bufferHeight);
        if(pointResult.isInside)
        {
            result = 1;
            break;
        }
    }

    return result;
}

void
RenderDot(ColorBuffer colorBuffer, uint32 x, uint32 y, uint32 radius, Color color)
{
    uint32 minX = x - radius;
    uint32 maxX = x + radius;
    uint32 minY = y - radius;
    uint32 maxY = y + radius;

    //real32 *rs = colorBuffer.rs + minX + (colorBuffer.width * minY);
    //real32 *rsCurrent = rs;

    for(uint32 row = minY; row <= maxY; row++)
    {
        real32 *rsCurrent = colorBuffer.rs + minX + (colorBuffer.width * row);
        real32 *gsCurrent = colorBuffer.gs + minX + (colorBuffer.width * row);
        real32 *bsCurrent = colorBuffer.bs + minX + (colorBuffer.width * row);
        
        for(uint32 col = minX; col <= maxX; col++)
        {
            *rsCurrent++ = color.r;
            *gsCurrent++ = color.g;
            *bsCurrent++ = color.b;
        }
    }
}

void
RenderModelsOptimized_8x(TransformedModel *transformedModels, uint32 count,
                         M4 projectionView, MemoryArena *arena, uint32 bufferWidth, uint32 bufferHeight,
                         ColorBuffer colorBuffer, real32 *zBuffer)
{
    //START_CYCLE_TICK(RENDER_METHOD);
    //START_CYCLE_TICK(GET_VALID_TRIANGLES);
    uint32 totalValidModelsForRendering = 0;
    uint32 totalValidTrianglesForRenderCheck = 0;
    for(uint32 i = 0; i < count; i++)
    {
        TransformedModel transformedModel = transformedModels[i];
        Box box = transformedModel.box;
        if(IsBoxVisible(box.min, box.max, projectionView, bufferWidth, bufferHeight))
        {
            totalValidTrianglesForRenderCheck += transformedModel.model.triCount;
            transformedModels[totalValidModelsForRendering++] = transformedModel;
        }
    }

    TriangleRenderData *triangles = (TriangleRenderData *)PushBuffer(arena,
                                                                     totalValidTrianglesForRenderCheck *
                                                                     sizeof(TriangleRenderData));
    uint32 totalValidTrianglesForRendering = 0;
    for(uint32 i = 0; i < totalValidModelsForRendering; i++)
    {
        TransformedModel transformedModel = transformedModels[i];
        uint32 triCount = transformedModel.model.triCount;
        V3 *verts = transformedModel.model.verts;
        V3 *vertexWorldPositions = transformedModel.vertexWorldPositions;
        uint32 *indices = transformedModel.model.indices;

        for(uint32 j = 0; j < triCount; j++)
        {
            V3 v0 = vertexWorldPositions[*indices++];
            V3 v1 = vertexWorldPositions[*indices++];
            V3 v2 = vertexWorldPositions[*indices++];

            V4 preClippedPoint0 = projectionView * v0;
            V4 preClippedPoint1 = projectionView * v1;
            V4 preClippedPoint2 = projectionView * v2;

            ScreenPointResult spr0 = GetScreenCoordinates(preClippedPoint0, bufferWidth, bufferHeight);
            ScreenPointResult spr1 = GetScreenCoordinates(preClippedPoint1, bufferWidth, bufferHeight);
            ScreenPointResult spr2 = GetScreenCoordinates(preClippedPoint2, bufferWidth, bufferHeight);

            if(spr0.isInside || spr1.isInside || spr2.isInside)
            {
                V3 sp0 = spr0.screenPoint;
                V3 sp1 = spr1.screenPoint;
                V3 sp2 = spr2.screenPoint;

                // NOTE: returns for clockwise (<0), anti-clockwise (>0) and collinear (=0).
                real32 signedAreaDouble = ((sp1.x - sp0.x)*(sp2.y - sp0.y)) - ((sp2.x - sp0.x)*(sp1.y - sp0.y));

                if(signedAreaDouble > 0)
                {
                    TriangleRenderData triangleRenderData;
                    triangleRenderData.worldPositions[0] = v0;
                    triangleRenderData.worldPositions[1] = v1;
                    triangleRenderData.worldPositions[2] = v2;

                    triangleRenderData.screenPoints[0] = sp0;
                    triangleRenderData.screenPoints[1] = sp1;
                    triangleRenderData.screenPoints[2] = sp2;

                    triangleRenderData.color = transformedModel.color;

                    triangles[totalValidTrianglesForRendering++] = triangleRenderData;
                }
            }
        }
    }

    //END_CYCLE_TICK(GET_VALID_TRIANGLES);
    
    //START_CYCLE_TICK(RENDER_VALID_TRIANGLES);

    real32 hMax = bufferWidth - 1;
    real32 vMax = bufferHeight - 1;
    __m256 _One = _mm256_set1_ps(1.0f);
    __m256 _Half = _mm256_set1_ps(0.5f);
    __m256 _AllZeros = _mm256_setzero_ps();
    __m256 _AllOnes = _mm256_cmp_ps(_AllZeros, _AllZeros, _CMP_EQ_OS);
    __m256 _Eight = _mm256_set1_ps(8.0f);
    __m256 _Max = _mm256_set_ps(hMax, hMax, vMax, vMax, 0.0f, 0.0f, 0.0f, 0.0f);

    //totalValidTrianglesForRendering = 3;
    Color colors[6] = { Color(1.0f, 1.0f, 1.0f, 1.0f), Color(1.0f, 0.0f, 0.0f, 1.0f),
                        Color(0.0f, 1.0f, 0.0f, 1.0f), Color(1.0f, 0.0f, 1.0f, 1.0f),
                        Color(0.0f, 0.0f, 0.0f, 1.0f), Color(1.0f, 1.0f, 0.0f, 1.0f) };

    for(uint32 t = 0; t < totalValidTrianglesForRendering; t++)
    {
        TriangleRenderData triangle = triangles[t];
        V3 *screenPoints = triangle.screenPoints;
        Color color = triangle.color;
        
        __m256 _Red = _mm256_set1_ps(color.r);
        __m256 _Green = _mm256_set1_ps(color.g);
        __m256 _Blue = _mm256_set1_ps(color.b);
        __m256 _Alpha = _mm256_set1_ps(color.a);

        real32 x1 = screenPoints[0].x;
        real32 y1 = screenPoints[0].y;
        real32 z1 = screenPoints[0].z;
        
        real32 x2 = screenPoints[1].x;
        real32 y2 = screenPoints[1].y;
        real32 z2 = screenPoints[1].z;
        
        real32 x3 = screenPoints[2].x;
        real32 y3 = screenPoints[2].y;
        real32 z3 = screenPoints[2].z;

        real32 y23 = y2 - y3;
        real32 y31 = y3 - y1;
        real32 x32 = x3 - x2;
        real32 x13 = x1 - x3;

        real32 bcDenInv = 1.0f / ((y23*x13) - (x32*y31));

        real32 y23Mul = y23 * bcDenInv;
        real32 y31Mul = y31 * bcDenInv;
        real32 x32Mul = x32 * bcDenInv;
        real32 x13Mul = x13 * bcDenInv;

        real32 xMin = ((x1 < x2) && (x1 < x3)) ? x1 : (x2 < x3 ? x2 : x3);
        real32 xMax = ((x1 > x2) && (x1 > x3)) ? x1 : (x2 > x3 ? x2 : x3);

        real32 yMin = ((y1 < y2) && (y1 < y3)) ? y1 : (y2 < y3 ? y2 : y3);
        real32 yMax = ((y1 > y2) && (y1 > y3)) ? y1 : (y2 > y3 ? y2 : y3);

        __m256 _Clamped = _mm256_set_ps(xMin, xMax, yMin, yMax, 0.0f, 0.0f, 0.0f, 0.0f);
        _Clamped = _mm256_min_ps(_mm256_max_ps(_Clamped, _AllZeros), _Max);
        __m256 _Truncated = _mm256_floor_ps(_Clamped);
        __m256 _Adjusted = _mm256_add_ps(_Truncated, _Half);        

        real32 *adjustedValues = (real32 *)&_Adjusted;
        real32 colMin = adjustedValues[7];
        real32 colMax = adjustedValues[6];
        real32 rowMin = adjustedValues[5];
        real32 rowMax = adjustedValues[4];

        real32 *truncatedValues = (real32 *)&_Truncated;
        uint32 strideRowBy8 = bufferWidth / 8;
        uint32 offset = truncatedValues[7] + (truncatedValues[5] * bufferWidth);
        __m256 *_RPtr = (__m256 *)(colorBuffer.rs + offset);
        __m256 *_GPtr = (__m256 *)(colorBuffer.gs + offset);
        __m256 *_BPtr = (__m256 *)(colorBuffer.bs + offset);
        __m256 *_APtr = (__m256 *)(colorBuffer.as + offset);
        __m256 *_ZPtr = (__m256 *)(zBuffer + offset);
        
        __m256 *_ZCurrent = _ZPtr;
        __m256 *_RCurrent = _RPtr;
        __m256 *_GCurrent = _GPtr;
        __m256 *_BCurrent = _BPtr;
        __m256 *_ACurrent = _APtr;

        __m256 _Y23Mul = _mm256_set1_ps(y23Mul);
        __m256 _Y31Mul = _mm256_set1_ps(y31Mul);
        __m256 _X32Mul = _mm256_set1_ps(x32Mul);
        __m256 _X13Mul = _mm256_set1_ps(x13Mul);

        __m256 _X3 = _mm256_set1_ps(x3);
        __m256 _Y3 = _mm256_set1_ps(y3);
        __m256 _Z1 = _mm256_set1_ps(z1);
        __m256 _Z2 = _mm256_set1_ps(z2);
        __m256 _Z3 = _mm256_set1_ps(z3);

        __m256 _PointY = _mm256_set1_ps(rowMin);
        __m256 _PointX = _mm256_set_ps(colMin, colMin+1.0f, colMin+2.0f, colMin+3.0f,
                                       colMin+4.0f, colMin+5.0f, colMin+6.0f, colMin+7.0f);
        __m256 _Py = _mm256_sub_ps(_PointY, _Y3);
        __m256 _Y23MulEight = _mm256_mul_ps(_Y23Mul, _Eight);
        __m256 _Y31MulEight = _mm256_mul_ps(_Y31Mul, _Eight);
        __m256 _Y23Y31EightSum = _mm256_add_ps(_Y23MulEight, _Y31MulEight);

        __m256 _Y23MulEightZ = _mm256_mul_ps(_Y23MulEight, _Z1);
        __m256 _Y31MulEightZ = _mm256_mul_ps(_Y31MulEight, _Z2);
        __m256 _Y23Y31EightSumZ = _mm256_mul_ps(_Y23Y31EightSum, _Z3);
        __m256 _AddZ = _mm256_sub_ps(_mm256_add_ps(_Y23MulEightZ, _Y31MulEightZ), _Y23Y31EightSumZ);

        for(real32 row = rowMin; row <= rowMax; row += 1.0f)
        {
            __m256 _Px = _mm256_sub_ps(_PointX, _X3);
            __m256 _Bc1 = _mm256_add_ps(_mm256_mul_ps(_Y23Mul, _Px), _mm256_mul_ps(_X32Mul, _Py));
            __m256 _Bc2 = _mm256_add_ps(_mm256_mul_ps(_Y31Mul, _Px), _mm256_mul_ps(_X13Mul, _Py));
            __m256 _Bc3 = _mm256_sub_ps(_One, _mm256_add_ps(_Bc1, _Bc2));
            //__m256 _ZValuePixel = _mm256_add_ps(_mm256_add_ps(_mm256_mul_ps(_Z1, _Bc1), _mm256_mul_ps(_Z2, _Bc2)),
            //_mm256_mul_ps(_Z3, _Bc3));
            
            for(real32 col = colMin; col <= colMax; col += 8.0f)
            {
                //START_CYCLE_TICK(RENDER_PIXEL); // 65% of Render_Triangle.
                
                __m256 _MaskBc1 = _mm256_and_ps(_mm256_cmp_ps(_Bc1, _AllZeros, _CMP_GE_OS),
                                                _mm256_cmp_ps(_Bc1, _One, _CMP_LE_OS));
                __m256 _MaskBc2 = _mm256_and_ps(_mm256_cmp_ps(_Bc2, _AllZeros, _CMP_GE_OS),
                                                _mm256_cmp_ps(_Bc2, _One, _CMP_LE_OS));
                __m256 _MaskBc3 = _mm256_and_ps(_mm256_cmp_ps(_Bc3, _AllZeros, _CMP_GE_OS),
                                                _mm256_cmp_ps(_Bc3, _One, _CMP_LE_OS));
                __m256 _ZValuePixel = _mm256_add_ps(_mm256_add_ps(_mm256_mul_ps(_Z1, _Bc1), _mm256_mul_ps(_Z2, _Bc2)),
                                         _mm256_mul_ps(_Z3, _Bc3));
                __m256 _ZValueCurrent = *_ZCurrent;
                __m256 _ZMask = _mm256_cmp_ps(_ZValuePixel, _ZValueCurrent, _CMP_LT_OS);
                __m256 _Mask = _mm256_and_ps(_mm256_and_ps(_MaskBc1, _MaskBc2), _mm256_and_ps(_MaskBc3, _ZMask));
                __m256 _MaskInv = _mm256_andnot_ps(_Mask, _AllOnes);

                *_ZCurrent++ = _mm256_or_ps(_mm256_and_ps(_ZValuePixel, _Mask), _mm256_and_ps(_ZValueCurrent, _MaskInv));
                *_RCurrent++ = _mm256_or_ps(_mm256_and_ps(_Red, _Mask), _mm256_and_ps(*_RCurrent, _MaskInv));
                *_GCurrent++ = _mm256_or_ps(_mm256_and_ps(_Green, _Mask), _mm256_and_ps(*_GCurrent, _MaskInv));
                *_BCurrent++ = _mm256_or_ps(_mm256_and_ps(_Blue, _Mask), _mm256_and_ps(*_BCurrent, _MaskInv));
                *_ACurrent++ = _mm256_or_ps(_mm256_and_ps(_Alpha, _Mask), _mm256_and_ps(*_ACurrent, _MaskInv));

                _Bc1 = _mm256_add_ps(_Bc1, _Y23MulEight);
                _Bc2 = _mm256_add_ps(_Bc2, _Y31MulEight);                         
                _Bc3 = _mm256_sub_ps(_Bc3, _Y23Y31EightSum);
                _ZValuePixel = _mm256_add_ps(_ZValuePixel, _AddZ);
                //END_CYCLE_TICK(RENDER_PIXEL);
            }
            
            _RPtr += strideRowBy8;
            _GPtr += strideRowBy8;
            _BPtr += strideRowBy8;
            _APtr += strideRowBy8;
            _ZPtr += strideRowBy8;
            _RCurrent = _RPtr;
            _GCurrent = _GPtr;
            _BCurrent = _BPtr;
            _ACurrent = _APtr;
            _ZCurrent = _ZPtr;

            _Py = _mm256_add_ps(_Py, _One);
        }

    }

    for(uint32 i = 0; i < totalValidTrianglesForRendering; i++)
    {
        RenderDot(colorBuffer, (uint32)triangles[i].screenPoints[0].x, (uint32)triangles[i].screenPoints[0].y, 5, colors[i]);
        RenderDot(colorBuffer, (uint32)triangles[i].screenPoints[1].x, (uint32)triangles[i].screenPoints[1].y, 5, colors[i]);
        RenderDot(colorBuffer, (uint32)triangles[i].screenPoints[2].x, (uint32)triangles[i].screenPoints[2].y, 5, colors[i]);
    }

    RenderDot(colorBuffer, 1000.0f, 540, 20, Color(1.0f, 0.0f, 0.0f, 1.0f));

    //END_CYCLE_TICK(RENDER_VALID_TRIANGLES);
    //END_CYCLE_TICK(RENDER_METHOD);
}

void
RenderModelsOptimized(TransformedModel *transformedModels, uint32 count,
                      M4 projectionView, MemoryArena *arena, uint32 bufferWidth, uint32 bufferHeight,
                      uint32 *pixels, real32 *zBuffer)
{
    uint32 totalValidModelsForRendering = 0;
    uint32 totalValidTrianglesForRenderCheck = 0;
    for(uint32 i = 0; i < count; i++)
    {
        TransformedModel transformedModel = transformedModels[i];
        Box box = transformedModel.box;
        if(IsBoxVisible(box.min, box.max, projectionView, bufferWidth, bufferHeight))
        {
            totalValidTrianglesForRenderCheck += transformedModel.model.triCount;
            transformedModels[totalValidModelsForRendering++] = transformedModel;
        }
    }

    TriangleRenderData *triangles = (TriangleRenderData *)PushBuffer(arena,
                                                                     totalValidTrianglesForRenderCheck *
                                                                     sizeof(TriangleRenderData));
    uint32 totalValidTrianglesForRendering = 0;
    for(uint32 i = 0; i < totalValidModelsForRendering; i++)
    {
        TransformedModel transformedModel = transformedModels[i];
        uint32 triCount = transformedModel.model.triCount;
        V3 *verts = transformedModel.model.verts;
        V3 *vertexWorldPositions = transformedModel.vertexWorldPositions;
        uint32 *indices = transformedModel.model.indices;
        
        for(uint32 j = 0; j < triCount; j++)
        {
            V3 v0 = vertexWorldPositions[*indices];
            indices++;
            V3 v1 = vertexWorldPositions[*indices];
            indices++;
            V3 v2 = vertexWorldPositions[*indices];
            indices++;

            V4 preClippedPoint0 = projectionView * v0;
            V4 preClippedPoint1 = projectionView * v1;
            V4 preClippedPoint2 = projectionView * v2;

            ScreenPointResult spr0 = GetScreenCoordinates(preClippedPoint0, bufferWidth, bufferHeight);
            ScreenPointResult spr1 = GetScreenCoordinates(preClippedPoint1, bufferWidth, bufferHeight);
            ScreenPointResult spr2 = GetScreenCoordinates(preClippedPoint2, bufferWidth, bufferHeight);

            if(spr0.isInside || spr1.isInside || spr2.isInside)
            {
                V3 sp0 = spr0.screenPoint;
                V3 sp1 = spr1.screenPoint;
                V3 sp2 = spr2.screenPoint;

                // NOTE: returns for clockwise (<0), anti-clockwise (>0) and collinear (=0).
                real32 signedAreaDouble = ((sp1.x - sp0.x)*(sp2.y - sp0.y)) - ((sp2.x - sp0.x)*(sp1.y - sp0.y));

                if(signedAreaDouble > 0)
                {
                    TriangleRenderData triangleRenderData;
                    triangleRenderData.worldPositions[0] = v0;
                    triangleRenderData.worldPositions[1] = v1;
                    triangleRenderData.worldPositions[2] = v2;

                    triangleRenderData.screenPoints[0] = sp0;
                    triangleRenderData.screenPoints[1] = sp1;
                    triangleRenderData.screenPoints[2] = sp2;

                    triangleRenderData.color = transformedModel.color;

                    triangles[totalValidTrianglesForRendering++] = triangleRenderData;
                }
            }
        }
    }

    for(uint32 t = 0; t < totalValidTrianglesForRendering; t++)
    {
        TriangleRenderData triangle = triangles[t];
        V3 *screenPoints = triangle.screenPoints;
        V3 *worldPositions = triangle.worldPositions;
        uint32 color = (uint32)triangle.color;

        real32 x1 = screenPoints[0].x;
        real32 y1 = screenPoints[0].y;
        real32 z1 = screenPoints[0].z;
        
        real32 x2 = screenPoints[1].x;
        real32 y2 = screenPoints[1].y;
        real32 z2 = screenPoints[1].z;
        
        real32 x3 = screenPoints[2].x;
        real32 y3 = screenPoints[2].y;
        real32 z3 = screenPoints[2].z;

        real32 y23 = y2 - y3;
        real32 y31 = y3 - y1;
        real32 x32 = x3 - x2;
        real32 x13 = x1 - x3;
        
        real32 bcDenInv = 1.0f / ((y23*x13) + (x32*y31));
        
        real32 y23Mul = y23 * bcDenInv;
        real32 y31Mul = y31 * bcDenInv;
        real32 x32Mul = x32 * bcDenInv;
        real32 x13Mul = x13 * bcDenInv;

        real32 xMin = x1 < x2 ? x1 : x2;
        real32 xMax = x1 > x2 ? x1 : x2;
        xMin = x3 < xMin ? x3 : xMin;
        xMax = x3 > xMax ? x3 : xMax;

        real32 yMin = y1 < y2 ? y1 : y2;
        real32 yMax = y1 > y2 ? y1 : y2;
        yMin = y3 < yMin ? y3 : yMin;
        yMax = y3 > yMax ? y3 : yMax;
        
        real32 xMinClamped = Clamp(xMin, 0.0f, (real32)bufferWidth-1);
        real32 xMaxClamped = Clamp(xMax, 0.0f, (real32)bufferWidth-1);
        real32 yMinClamped = Clamp(yMin, 0.0f, (real32)bufferHeight-1);
        real32 yMaxClamped = Clamp(yMax, 0.0f, (real32)bufferHeight-1);
        
        uint32 strideRow = bufferWidth;
        uint32 offset = (uint32)xMinClamped + ((uint32)yMinClamped * strideRow);
        uint32 *rowPtr = pixels + offset;
        real32 *zRowPtr = (real32 *)((uint32 *)zBuffer + offset);
        uint32 *current = rowPtr;
        real32 *zCurrent = zRowPtr;

        uint32 rowMin = yMinClamped;
        uint32 rowMax = yMaxClamped + 1;
        for(uint32 row = rowMin; row < rowMax; row++)
        {
            real32 rowMid = row + 0.5f;
            bool8 enteredTriangle = 0;
            bool8 leftEntered = 0;
            bool8 rightEntered = 0;
            uint32 colMin = xMinClamped;
            uint32 colMax = xMaxClamped+1;
            for(uint32 col = colMin; col < colMax; col++)
            {                
                V2 point = { col+0.5f, rowMid };
                real32 px3 = point.x - x3;
                real32 py3 = point.y - y3;
                
                real32 bc1 = (y23Mul * px3) + (x32Mul * py3);
                real32 bc2 = (y31Mul * px3) + (x13Mul * py3);
                real32 bc3 = 1.0f - bc1 - bc2;
                    
                if(bc1 >= 0.0f && bc1 <= 1.0f &&
                   bc2 >= 0.0f && bc2 <= 1.0f &&
                   bc3 >= 0.0f && bc3 <= 1.0f)
                {
                    real32 pointZ = (z1 * bc1) + (z2 * bc2) + (z3 * bc3);
                    if((*zCurrent) > pointZ)
                    {
                        *zCurrent = pointZ;
                        *current = (uint32)color;
                    }

                    if(enteredTriangle == 0)
                    {
                        enteredTriangle = 1;
                    }

                }
                else if(enteredTriangle)
                {
                    break;
                }
                
                current++;
                zCurrent++;
            }

            rowPtr += strideRow;
            current = rowPtr;

            zRowPtr += strideRow;
            zCurrent = zRowPtr;
        }
        
    }
}

IntegerToStringResult
UInt32ToString(uint32 value, MemoryArena *arena)
{
    uint32 comparer = 1;
    uint32 totalDigits = 0;
    uint8 *digitsAscii = (uint8 *)PushBuffer(arena, sizeof(uint8) * 10);
    digitsAscii[0] = '0';
    for(uint32 i = 0; i < 10; i++)
    {
        if(value > 0)
        {
            totalDigits++;
            uint32 quotient = value / 10;
            uint32 onesRemoved = quotient * 10;
            uint32 lastDigit = value - onesRemoved;
            value = (uint32)quotient;
            digitsAscii[i] = lastDigit + 48;
        }
    }

    IntegerToStringResult result;
    result.digitCount = totalDigits;
    result.digitsAscii = digitsAscii;
    return result;
}

// TODO: Real number coordinates.
void RenderButton(int32 x, int32 y, uint32 width, uint32 height,
                  uint32 bufferWidth, uint32 bufferHeight, uint32 *pixels, Color albedo = Color(255))
{
    uint32 minX = Clamp(x,               0, bufferWidth  - 1);
    uint32 maxX = Clamp(x + width, 0, bufferWidth  - 1);
    uint32 minY = Clamp(y,               0, bufferHeight - 1);
    uint32 maxY = Clamp(y + height,0, bufferHeight - 1);
    int32 offsetX = minX - x;
    int32 offsetY = minY - y;

    uint32 *pixelRow = pixels + (minY * bufferWidth) + minX;
    for(uint32 r = minY; r < maxY; r++)
    {
        uint32 *pixel = pixelRow;
        for(uint32 c = minX; c < maxX; c++)
        {
            Color bitmapColor = albedo;
            Color pixelColor = U32ToColor(*pixel);
            Color finalColor = LerpColor(bitmapColor, pixelColor, bitmapColor.a / 255.0f);
            finalColor.a = 255;
            *pixel = (uint32)finalColor;
            pixel++;
        }
        
        pixelRow += bufferWidth;
    }
}

void
RenderUIElements(UIElement *uiElements, uint32 uiElementCount,
                 uint32 bufferWidth, uint32 bufferHeight, uint32 *pixels)
{
    for(uint32 i = 0; i < uiElementCount; i++)
    {
        UIElement uiElement = uiElements[i];
        switch(uiElement.type)
        {
            case UIElement_Button:
            {
                RenderButton(uiElement.leftX, uiElement.bottomY, uiElement.width, uiElement.height,
                             bufferWidth, bufferHeight, pixels, uiElement.color);
            }
            break;
            case UIElement_TextBox:
            {
            }
            break;
        }
    }
}

// TODO: Real number coordinates.
void
Render2DQuad(int32 x, int32 y, Color albedo,
             uint32 bitmapWidth, uint32 bitmapHeight, uint32 bitsPerPixel, Color *bitmapPixels,
             uint32 bufferWidth, uint32 bufferHeight, uint32 *pixels)
{
    uint32 minX = Clamp(x,               0, bufferWidth  - 1);
    uint32 maxX = Clamp(x + bitmapWidth, 0, bufferWidth  - 1);
    uint32 minY = Clamp(y,               0, bufferHeight - 1);
    uint32 maxY = Clamp(y + bitmapHeight,0, bufferHeight - 1);
    int32 offsetX = minX - x;
    int32 offsetY = minY - y;

    uint32 *pixelRow = pixels + (minY * bufferWidth) + minX;
    Color *bitmapPixelRow = bitmapPixels + (offsetY * bitmapWidth) + offsetX;
    for(uint32 r = minY; r < maxY; r++)
    {
        uint32 *pixel = pixelRow;
        Color *bitmapPixel = bitmapPixelRow;
        for(uint32 c = minX; c < maxX; c++)
        {
            Color bitmapColor = albedo * (*bitmapPixel);
            Color pixelColor = U32ToColor(*pixel);
            Color finalColor = LerpColor(bitmapColor, pixelColor, bitmapColor.a / 255.0f);
            finalColor.a = 255;
            *pixel = (uint32)finalColor;
            pixel++;
            bitmapPixel++;
        }
        
        pixelRow += bufferWidth;
        bitmapPixelRow += bitmapWidth;
    }
}

void
PrintValueUInt32(uint32 value, int32 x, int32 y, Color fontColor,
                 LoadedBitmap *glyphs, uint32 startAscii, uint32 totalGlyphs,
                 uint32 bufferWidth, uint32 bufferHeight, uint32 *pixels, MemoryArena *arena)
{
    IntegerToStringResult integerToStringResult = UInt32ToString(value, arena);
    uint32 digitCount = integerToStringResult.digitCount;
    uint8 *digitsAscii = integerToStringResult.digitsAscii;
    int32 currentX = x;
    int32 currentY = y;
    for(int32 i = digitCount-1; i >= 0; i--)
    {
        uint32 letterIndex = digitsAscii[i] - startAscii;
        LoadedBitmap letterTex = glyphs[letterIndex];
        Render2DQuad(currentX, currentY, fontColor,
                     letterTex.width, letterTex.height, letterTex.bitsPerPixel, letterTex.data,
                     bufferWidth, bufferHeight, pixels);
        currentX += letterTex.width;
    }
}

void
PrintLine(char *line, int32 x, int32 y, Color fontColor,
          LoadedBitmap *glyphs, uint32 startAscii, uint32 totalGlyphs, uint32 bufferWidth, uint32 bufferHeight,
          uint32 *pixels)
{
    uint32 index = 0;
    int32 currentX = x;
    int32 currentY = y;
    while(line[index] != 0)
    {
        if(line[index] >= startAscii)
        {
            uint32 letterIndex = line[index] - startAscii;
            LoadedBitmap letterTex = glyphs[letterIndex];
            Render2DQuad(currentX, currentY, fontColor,
                         letterTex.width, letterTex.height, letterTex.bitsPerPixel, letterTex.data,
                         bufferWidth, bufferHeight, pixels);
            currentX += letterTex.width;
        }
        else
            currentX += 8;
        
        index++;
    }
}

// NOTE: Non SIMD version seems to be running faster.
#if 0
void
ClearBuffer(real32 *zBuffer, real32 val, uint32 length)
{
    __m256 _Val = _mm256_set1_ps(val);
    __m256 *_Zbuffer = (__m256 *)zBuffer;
    uint32 sets = length / 8;
    for(uint32 i = 0; i < sets; i++)
    {
        *_Zbuffer++ = _Val;
    }
}
#else
void
ClearBuffer(real32 *buffer, real32 val, uint32 length)
{
    for(uint32 i = 0; i < length; i++)
    {
        *buffer++ = val;
    }
}
#endif

#if 0
void
ClearBuffer(ColorBuffer colorBuffer, Color color)
{
    uint32 length = (colorBuffer.width * colorBuffer.height) / 8;
    __m256 *channels = (__m256 *)colorBuffer.rs;
    for(uint32 channel = 0; channel < 4; channel++)
    {
        __m256 _Value = _mm256_set1_ps(color.v[channel]);
        for(uint32 i = 0; i < length; i++)
        {
            *channels++ = _Value;
        }
    }
}
#else
void
ClearBuffer(ColorBuffer colorBuffer, Color color)
{
    uint32 length = colorBuffer.width * colorBuffer.height;
    real32 *channels = colorBuffer.rs;
    for(uint32 channel = 0; channel < 4; channel++)
    {
        real32 value = color.v[channel];
        for(uint32 i = 0; i < length; i++)
        {
            *channels++ = value;
        }
    }
}
#endif

#endif
