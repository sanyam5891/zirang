#if !defined(ZIRANG_MATRIX_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#define ZIRANG_MATRIX_H
#include "zirang_vector.h"

struct M3
{
    V3 cols[3];
};

struct M4
{
    V4 cols[4];
};

inline M4
operator*(M4 m0, M4 m1)
{
    M4 result = {};
    for(int32 rowm0 = 0; rowm0 < 4; rowm0++)
    {   
        for(int32 colm1 = 0; colm1 < 4; colm1++)
        {
            real32 val = 0.0f;
            for(int32 i = 0; i < 4; i++)
            {
                val += (m0.cols[i].e[rowm0] * m1.cols[colm1].e[i]);
            }

            result.cols[colm1].e[rowm0] = val;
        }
    }

    return result;
}

inline V4
operator*(M4 m4, V4 v4)
{
    V4 result = {};
    for(int32 rowm0 = 0; rowm0 < 4; rowm0++)
    {   
        for(int32 colm1 = 0; colm1 < 1; colm1++)
        {
            real32 val = 0.0f;
            for(int32 i = 0; i < 4; i++)
            {
                val += (m4.cols[i].e[rowm0] * v4.e[i]);
            }

            result.e[rowm0] = val;
        }
    }

    return result;
}

inline V4
operator*(M4 m4, V3 v3)
{
    V4 v4 = { v3.x, v3.y, v3.z, 1.0f };
    V4 result = m4 * v4;
    return result;
}

M4
IdentityMatrix()
{
    V4 col0 = {1.0f, 0.0f, 0.0f, 0.0f};
    V4 col1 = {0.0f, 1.0f, 0.0f, 0.0f};
    V4 col2 = {0.0f, 0.0f, 1.0f, 0.0f};
    V4 col3 = {0.0f, 0.0f, 0.0f, 1.0f};

    M4 result = {col0, col1, col2, col3};
    return result;
}

#endif
