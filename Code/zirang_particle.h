#if !defined(ZIRANG_PARTICLE_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#define PARTICLE_SYSTEM 1

#if PARTICLE_SYSTEM

struct Particle
{
    bool8 isActive;
    
    Transform transform;
    real32 lifetime;
    real32 age;
    V3 velocity;
    Color albedo;
    LoadedBitmap *tex;
};

struct ParticleEmitter
{
    bool8 enabled;
    
    Transform transform;
    uint32 maxParticles;
    real32 lifetime;
    real32 maxSpeed;
    Color albedo;
    LoadedBitmap *tex;

    Particle *particles;
};

void
InitializeParticleEmitter(ParticleEmitter *particleEmitter, V3 axis)
{
    // NOTE: Generating new particles.
    ParticleEmitter emitter = *particleEmitter;
    Particle *particles = emitter.particles;       
    uint32 particlesToGenerate = emitter.maxParticles;
    real32 angleDelta = 360.0f / (real32)particlesToGenerate;
    real32 currentAngle = 0.0f;
    V3 startDirection = v3(0.0f, 1.0f, 0.0f);
    
    for(uint32 i = 0; i < particlesToGenerate; i++)
    {
        V3 currentDirection = Normalize(*((V3*)&(RotateAxis(axis, currentAngle) * startDirection)));
        Particle newParticle = {};
        newParticle.isActive = 1;
        newParticle.transform = emitter.transform;
        newParticle.lifetime = emitter.lifetime;
        newParticle.age = 0;
        newParticle.velocity = emitter.maxSpeed * currentDirection;
        newParticle.albedo = emitter.albedo;
        newParticle.tex = emitter.tex;
        particles[i] = newParticle;

        currentAngle += (angleDelta);
    }

    particleEmitter->enabled = 1;
}

void
SimulateParticles(ParticleEmitter *particleEmitter, real32 deltaTime)
{
    ParticleEmitter emitter = *particleEmitter;
    Particle *particles = emitter.particles;
    
    // NOTE: Simulating the particles that are already active
    uint32 totalParticles = emitter.maxParticles;
    for(uint32 i = 0; i < totalParticles; i++)
    {
        Particle particle = particles[i];
        particle.age += deltaTime;
        if(particle.isActive && particle.age < particle.lifetime)
        {
            particle.transform.position += (deltaTime * particle.velocity);
        }
        else
        {
            particle.isActive = 0;
        }

        particles[i] = particle;
    }

    *particleEmitter = emitter;
}
#endif

#define ZIRANG_PARTICLE_H
#endif
