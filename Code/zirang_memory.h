#if !defined(ZIRANG_MEMORY_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

struct MemoryArena
{
    bool32 isCircular;
    void *base;
    void *end;
    void *current;
    uint32 used;
    uint32 size;
};

void *
PushBuffer(MemoryArena *arena, uint32 sizeInBytes)
{
    void *result;
    uint32 memoryLeft = (uint8 *)arena->end - (uint8 *)arena->current;
    if(memoryLeft > sizeInBytes)
    {
        result = arena->current;
        arena->current = ((uint8 *)arena->current) + sizeInBytes;
        arena->used = (uint8 *)arena->current - (uint8 *)arena->base;
    }
    else if(memoryLeft == sizeInBytes)
    {
        result = arena->current;
        arena->current = arena->base;
        arena->used = arena->size;
    }
    else
    {
        if(arena->isCircular)
        {
            result = arena->base;
            arena->current = ((uint8 *)arena->base) + sizeInBytes;
        }
        else
        {
            Assert(0);
        }
    }

    if(arena->used > arena->size)
        Assert(0);
    
    return result;
}

void
CopyBuffer(void *src, void *dst, uint32 bytes)
{
    uint8 *src8 = (uint8 *)src;
    uint8 *dst8 = (uint8 *)dst;
    for(uint32 i = 0; i < bytes; i++)
    {
        *dst8++ = *src8++;
    }
}

#define ZIRANG_MEMORY_H
#endif
