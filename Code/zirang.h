#if !defined(ZIRANG_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#define ZIRANG_H

/* - The app shall have a home menu with these options: Play, Highscores, Exit.
   - The game shall be about a plane inside an asteroid belt.
   - The goal is to destroy as many asteroids before dying/exiting.
   - There shall be bullets, asteroid explosion and plane explosion.
   - The app needs to run on Windows only.
   - The idea is to make a complete game from scratch with a little bit of many things.
*/

/* List of features for the elementary version of the game:
   - Software rendered (30 fps, SIMD)
   - Hardware rendered
   - Procedural generation of asteroids
   - Texture mapping (Jet)
   - Lighting
   - Shadows
   - Fonts
   - Particle effects (explosion, maybe jet tailpipe)
   - Fullscreen (toggle)
   - Custom cursor (Optional)
   - Keyboard & Xbox controller
   - Memory cap
   - Collision detection
   - Sound
   - Interactive & HUD UI
*/

/* Next steps:
   1. Explosion particle effects on plane collision.
*/

/* TODO:
   - Understand "static" and see where you have to use them in your code.
   - Create a debug and release version of your code.
   - Implement random number generator.
   - Estimate "temp" memory max usage per frame.
   - Try barycentric approach for "IsPointInsideTriangle".
*/

#include "zirang_platform.h"

#include "zirang_memory.h"
#include "zirang_math.h"
#include "zirang_file.h"
#include "zirang_vector.h"
#include "zirang_color.h"

enum ScreenType
{
    ScreenType_MainMenu,
    ScreenType_Game,
    ScreenType_Count
};

struct Screen
{
    ScreenType type;
    void (*Update)(GameData gameData);
};

struct ColorBuffer
{
    uint32 width;
    uint32 height;
    real32 *rs;
    real32 *gs;
    real32 *bs;
    real32 *as;
};

struct LoadedBitmap
{
    uint32 width;
    uint32 height;
    uint32 bitsPerPixel;
    Color *data;
    //Channels channels;
};

struct Model3D
{
    uint32 vertCount;
    uint32 triCount;
    
    V3 *verts;
    uint32 *indices;
    V2 *uvs;
    
    LoadedBitmap *colorTex;
};

enum UIElementType
{
    UIElement_Button,
    UIElement_TextBox
};

struct UIElement
{
    UIElementType type;
    uint32 leftX;
    uint32 bottomY;
    uint32 width;
    uint32 height;
    Color color;
    Color textColor;
    void (*onClick)();
    char *text;
};

struct Box
{
    V3 min;
    V3 max;
    V3 cente;
};

struct TransformedModel
{
    Model3D model;
    Box box;
    V3 *vertexWorldPositions;
    Color color;
};

#include "zirang_obj_parser.h"
#include "zirang_bmp_parser.h"
#include "zirang_transform.h"
#include "zirang_render_group.h"
#include "zirang_particle.h"

#define SINGLE_ASTEROID 1
#define DUMMY_ASTEROID 0

#if DUMMY_ASTEROID
#define MAX_ASTEROIDS 1
#else
#define MAX_ASTEROIDS 100000
#endif

#if SINGLE_ASTEROID
#undef MAX_ASTEROIDS
#define MAX_ASTEROIDS 1
#endif

#define GLYPH_COUNT 94
#define GLYPH_START_ASCII 33
#define MIN_INT -2147483648
#define MAX_INT 2147483647
#define MAX_UINT 4294967296

struct Camera
{
    Transform transform;
    real32 fov;
    real32 nPlane;
    real32 fPlane;
    real32 height;
    V3 front;
    V3 left;
};

struct FighterJet
{
    bool8 alive;
    Transform transform;
    real32 mass;
    real32 thrustWeightRatio;
    real32 maneuverMultiplier;
    V3 gravityForce;
    V3 liftForce;
    V3 thrustForce;
    V3 dragForce;
    V3 velocity;
    real32 moveSpeed;
    real32 tiltSpeed;
    real32 tiltMax;
    real32 tiltAngle;
    real32 tiltDampener;
    V3 forward;
    V3 right;
    V3 up;
    Color color;
};

struct Asteroid
{
    Transform transform;
    bool32 isActive;
    V3 rotationAxis;
    real32 rotationSpeed;
    real32 currentRotationAngle;
    V3 direction;
    real32 speed;
    Color color;
};

struct AsteroidGenerator
{
    real32 boundSize;
    V3 minBound;
    V3 maxBound;
    uint32 totalActive;
    Asteroid asteroids[MAX_ASTEROIDS];
};

enum GameScreenUI
{
    GameScreenUI_SteerLeftButton = 0,
    GameScreenUI_SteerRightButton = 1,
    GameScreenUI_Count
};

struct Game
{
    Screen screens[ScreenType_Count];
    void (*Update)(GameData);
    MemoryArena tempArena;
    MemoryArena permArena;
    real32 *zBuffer;
    //Color *colorBuffer0;
    //Color *colorBuffer1;
    ColorBuffer colorBuffers[2];
    Camera camera;
    FighterJet playerJet;
    AsteroidGenerator asteroidGenerator;

#if PARTICLE_SYSTEM
    ParticleEmitter explosionEmitter;
#endif
    
    LoadedBitmap explosionParticleTex;
    LoadedBitmap glyphs[GLYPH_COUNT]; // NOTE: Starting with 33 (decimal).
    Model3D quadModel;
    Model3D fighterJetModel;
    Model3D asteroidModel;
    real32 angle;

    UIElement uiElements[GameScreenUI_Count];
};

void
MainMenuScreenUpdate(GameData gameData);

void
GameScreenUpdate(GameData gameData);

#endif
