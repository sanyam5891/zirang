#if !defined(ZIRANG_OBJ_PARSER_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#include "zirang_string.h"
#include "zirang_file.h"

uint32
GetFaceIndex(char *word, uint32 len)
{
    uint32 result = 0;
    
    for(int i = 0; i < len; i++)
    {
        char c = word[i];
        if(c >= 48 && c <= 57) // Integer.
        {
            result = (result * 10) + (c-48);
        }
        else // Non-number.
        {
            break;
        }
    }
    
    return result;
}

void
GetObjMeshData(uint32 objSize, void *objData,
               uint32 mtlSize, void *mtlData,
               Model3D *modelData, MemoryArena *arena)
{
    FileWordsData mtlFileWordsData = GetFileWords(mtlSize, mtlData, arena);
    FileWordsData objFileWordsData = GetFileWords(objSize, objData, arena);

    uint32 vertCount = 0;
    uint32 triCount = 0;
    uint32 wordCount = objFileWordsData.count;
    for(int i = 0; i < wordCount; i++)
    {
        uint32 wordLen = objFileWordsData.lengths[i];
        char *word = (char *)(*(objFileWordsData.ptrs + i));
        
        if(IsWordEqual("v", word, 1, wordLen))
        {
            vertCount++;
        }
        else if(IsWordEqual("f", word, 1, wordLen))
        {
            triCount++;
        }
    }

    V3 *vertices = (V3 *)PushBuffer(arena, vertCount * sizeof(V3));
    V3 *normals = (V3 *)PushBuffer(arena, triCount * sizeof(V3));
    uint32 *indices = (uint32 *)PushBuffer(arena, triCount * 3 * sizeof(uint32));
    uint32 vertIndex = 0;
    uint32 triVertIndex = 0;

    for(int i = 0; i < wordCount; i++)
    {
        uint32 wordLen = objFileWordsData.lengths[i];
        char *word = (char *)(*(objFileWordsData.ptrs + i));
        char *word1 = (char *)(*(objFileWordsData.ptrs + i + 1));
        char *word2 = (char *)(*(objFileWordsData.ptrs + i + 2));
        char *word3 = (char *)(*(objFileWordsData.ptrs + i + 3));
        uint32 word1Length = objFileWordsData.lengths[i+1];
        uint32 word2Length = objFileWordsData.lengths[i+2];
        uint32 word3Length = objFileWordsData.lengths[i+3];

        if(IsWordEqual("v", word, 1, wordLen))
        {
            vertices[vertIndex++] = { GetFloat(word1, word1Length),
                                      GetFloat(word2, word2Length),
                                      GetFloat(word3, word3Length) };
        }
        else if(IsWordEqual("f", word, 1, wordLen))
        {
            indices[triVertIndex++] = GetFaceIndex(word1, word1Length)-1;
            indices[triVertIndex++] = GetFaceIndex(word2, word2Length)-1;
            indices[triVertIndex++] = GetFaceIndex(word3, word3Length)-1;
        }
    }

    modelData->vertCount = vertCount;
    modelData->triCount = triCount;
    modelData->verts = vertices;
    modelData->indices = indices;
    modelData->uvs = 0;
    modelData->colorTex = 0;
}

#define ZIRANG_OBJ_PARSER_H
#endif
