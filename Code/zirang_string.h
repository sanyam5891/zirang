#if !defined(ZIRANG_STRING_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

bool32
IsWordEqual(char *word0, char *word1, uint32 len)
{
    for(int i = 0; i < len; i++)
    {
        if(word0[i] != word1[i])
        {
            return 0;
        }
    }

    return 1;
}

bool32
IsWordEqual(char *word0, char *word1, uint32 len0, uint32 len1)
{
    if(len0 != len1)
    {
        return 0;
    }
    else
    {
        return IsWordEqual(word0, word1, len0);
    }
}

bool32
IsWordEqual(char *word0, char *word1)
{
    uint32 index = 0;
    while(word0[index] != '\0')
    {
        if(word0[index] != word1[index])
        {
            return 0;
        }

        index++;
    }
    return 1;
}

real32
GetFloat(char *word, uint32 len)
{
    bool32 foundDecimal = 0;
    bool32 isNegative = 0;
    uint32 minorCount = 1;
    real32 result = 0;
    for(int i = 0; i < len; i++)
    {
        char c = word[i];
        if(c == '.')
        {
            foundDecimal = 1;
        }
        else if(c == '-')
        {
            isNegative = 1;
        }
        else
        {
            uint32 num = c - 48;
            if(foundDecimal)
            {
                if(minorCount < 5)
                {
                    real32 den = pow(10, minorCount);
                    real32 minorVal = ((real32)num) / den;
                    result += minorVal;
                    minorCount++;
                }
                else
                {
                    break;
                }
            }
            else
            {
                result = (result * 10) + num;
            }
        }
    }

    if(isNegative)
        result *= -1;

    return result;
}

#define ZIRANG_STRING_H
#endif
