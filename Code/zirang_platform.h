#if !defined(ZIRANG_PLATFORM_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */
/*
#ifdef _cplusplus
extern "C" {
#endif
*/
#include <stdint.h>
#include <intrin.h>

#define REF_WIDTH 1920
#define REF_HEIGHT 1080

typedef int64_t int64;
typedef int32_t int32;
typedef int16_t int16;
typedef int8_t int8;

typedef uint64_t uint64;
typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t uint8;

typedef float real32;
typedef double real64;

typedef uint8 bool8;
typedef uint16 bool16;
typedef uint32 bool32;
typedef uint64 bool64;
    
#ifdef _MSC_VER
#define COMPILER_MSVC 1
#endif

#ifndef COMPILER_MSVC
#define COMPILER_MSVC 0
#endif

#define KB 1024
#define MB 1024 * KB
#define GB 1024 * MB
    
#define ARRAY_COUNT(x,y) sizeof(x)/sizeof(y)

#if ZIRANG_DEBUG
#define Assert(x) if(!x) {*((int *)0) = 0;}
#else
#define Assert(x)
#endif
    
#if ZIRANG_DEBUG
enum
{
    DEBUG_CYCLE_COUNTER_GAME_UPDATE_AND_RENDER = 0,
    DEBUG_CYCLE_COUNTER_RENDER_METHOD,
    DEBUG_CYCLE_COUNTER_GET_VALID_TRIANGLES,
    DEBUG_CYCLE_COUNTER_RENDER_VALID_TRIANGLES,
    DEBUG_CYCLE_COUNTER_RENDER_PIXEL,
    DEBUG_CYCLE_COUNTER_MEMORY_ACCESS,
    CYCLE_ID_COUNT
};

struct DebugCycleCounter
{
    uint64 cycles;
    uint64 hits;
    uint64 highestCycles;
    uint64 highestHits;
    uint64 highestCyclesPerHit;
};

extern struct GameMemory *debugGameMemory;

#if COMPILER_MSVC
#define START_CYCLE_TICK(ID) uint64 start##ID = __rdtsc();
#define END_CYCLE_TICK(ID)                                              \
    DebugCycleCounter *counter##ID = &debugGameMemory->debugCycleCounters[DEBUG_CYCLE_COUNTER_##ID]; \
    counter##ID->cycles += (__rdtsc() - start##ID);                     \
    counter##ID->hits++;
#else
#define START_CYCLE_TICK(ID)
#define END_CYCLE_TICK(ID)
#endif
    
#else
#define START_CYCLE_TICK(ID)
#define END_CYCLE_TICK(ID)
#endif
// NOTE: Services that the platform provides to the
// game. Eg. Reading/Writing files, Providing memory etc.

struct PlatformReadFileResult
{
    bool64 success;
    uint32 size;
    void *data;
};

#define PLATFORM_GET_MEMORY(x) void *x(uint32 bytes)
typedef PLATFORM_GET_MEMORY(platform_get_memory);

#define PLATFORM_GET_FILE_SIZE(x) uint32 x(char *fileName)
typedef PLATFORM_GET_FILE_SIZE(platform_get_file_size);

#define PLATFORM_READ_FILE(x) void x(char *fileName, PlatformReadFileResult *result, void *memory)
typedef PLATFORM_READ_FILE(platform_read_file);

struct PlatformFunctions
{
    platform_get_memory *PlatformGetMemory;
    platform_get_file_size *PlatformGetFileSize;
    platform_read_file *PlatformReadFile;
};


// NOTE: Services that the game provides to the platform. Eg. Filling up
// pixel buffers, sound buffers etc.

struct GameBuffer
{
    uint32 *pixels;
    uint32 width;
    uint32 height;
    uint32 bytesPerPixel;
};

struct GameMemory
{
    uint32 permanentMemorySize;
    void *permanentMemory;
    uint32 transientMemorySize;
    void *transientMemory;

#if ZIRANG_DEBUG
    DebugCycleCounter debugCycleCounters[CYCLE_ID_COUNT];    
#endif
};

struct GameTiming
{
    real32 deltaTimeMs;
    real32 deltaTime;
};

struct ControllerInput
{
    int16 lAnalogX;
};

struct GameButton
{
    bool8 isDown;
};

struct MouseInput
{
    bool8 isPressed;
    real32 oldPosX, oldPosY;
    real32 newPosX, newPosY;
    bool8 isFirst;
    bool8 isContinued;
};

struct GameInput
{
    bool32 isController;
    union
    {
        struct Buttons
        {
            GameButton left;
            GameButton right;
            GameButton up;
            GameButton down;
            GameButton space;
        } buttons;
        struct
        {
            GameButton gameButtons[sizeof(Buttons) / sizeof(GameButton)];
        };
    };
    ControllerInput controller;
    MouseInput mouseInput;
};

struct GameData
{
    GameBuffer *buffer;
    GameTiming timing;
    GameMemory *memory;
    GameInput *input;

    PlatformFunctions platform;
};

#define GAME_UPDATE_AND_RENDER(x) void x(GameData gameData)
typedef GAME_UPDATE_AND_RENDER(game_update_and_render);

#define GAME_GET_TEMP_MEMORY(x) void *x(uint32 bytes)
typedef GAME_GET_TEMP_MEMORY(game_get_temp_memory);    

struct GameFunctions
{
    game_update_and_render *GameUpdateAndRender;
    game_get_temp_memory *GameGetTempMemory;
};
/*
#ifdef _cplusplus
}
#endif
*/
#define ZIRANG_PLATFORM_H
#endif
