/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */
#include <windows.h>
#include <windowsx.h>
#include <dsound.h>
#include <xinput.h>

#include "zirang_platform.h"
#include "win32_zirang.h"

#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPGUID lpGuid, LPDIRECTSOUND* ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);

LPDIRECTSOUNDBUFFER secondarySoundBuffer;

// TODO: Though we don't know yet, but our intuitive goal is to not
// have any global variables.
uint8 isRunning;
Win32WindowsBuffer windowsBuffer;
Win32Dimensions windowDims;
WINDOWPLACEMENT g_wpPrev = { sizeof(g_wpPrev) };
GameInput gameInput;
GameFunctions gameFunctions;

// TODO: Error logging & handling.
void*
PlatformGetMemory(uint32 bytes)
{
    void *memory = VirtualAlloc(0, bytes, MEM_COMMIT, PAGE_READWRITE);
    return memory;
}

// TODO: Error logging & handling.
uint32
PlatformGetFileSize(char *fileName)
{
    uint32 result = 0;
    HANDLE file = CreateFileA(fileName,
                              GENERIC_READ | GENERIC_WRITE,
                              0, 0, OPEN_EXISTING,
                              FILE_ATTRIBUTE_NORMAL, 0);
    if(file != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER fileSize;
        GetFileSizeEx(file, &fileSize);
        result = fileSize.QuadPart;
    }

    BOOL closeStatus = CloseHandle(file);

    return result;
}

// TODO: Error logging & handling.
void
PlatformReadFile(char *fileName, PlatformReadFileResult *result, void *memory)
{
    result->success = 0;
    
    HANDLE file =  CreateFileA(fileName,
                               GENERIC_READ | GENERIC_WRITE,
                               0,
                               0,
                               OPEN_EXISTING,
                               FILE_ATTRIBUTE_NORMAL,
                               0);
    
    if(file != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER fileSize;
        GetFileSizeEx(file, &fileSize);
        result->size = fileSize.QuadPart;

        result->data = memory;
        if(result->data)
        {
            DWORD bytesRead;
            if(ReadFile(file, result->data, result->size, &bytesRead, 0))
            {
                if(bytesRead == result->size)
                {
                    result->success = 1;
                }
            }
        }
    }

    CloseHandle(file);
}

void
Win32ToggleFullscreen(HWND hwnd)
{
  DWORD dwStyle = GetWindowLong(hwnd, GWL_STYLE);
  if (dwStyle & WS_OVERLAPPEDWINDOW) {
    MONITORINFO mi = { sizeof(mi) };
    if (GetWindowPlacement(hwnd, &g_wpPrev) &&
        GetMonitorInfo(MonitorFromWindow(hwnd,
                       MONITOR_DEFAULTTOPRIMARY), &mi)) {
      SetWindowLong(hwnd, GWL_STYLE,
                    dwStyle & ~WS_OVERLAPPEDWINDOW);
      SetWindowPos(hwnd, HWND_TOP,
                   mi.rcMonitor.left, mi.rcMonitor.top,
                   mi.rcMonitor.right - mi.rcMonitor.left,
                   mi.rcMonitor.bottom - mi.rcMonitor.top,
                   SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
    }
  } else {
    SetWindowLong(hwnd, GWL_STYLE,
                  dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(hwnd, &g_wpPrev);
    SetWindowPos(hwnd, NULL, 0, 0, 0, 0,
                 SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                 SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
  }
}

// NOTE: 1. Looks only in a fixed location. 2. Doesn't try to reload
// on failure.

//TODO: Error logging & handling.
bool32
Win32LoadGameCodeDLL()
{
    bool32 result = 0;
    HMODULE gameCodeDLL = LoadLibraryA("zirang.dll");
    if(gameCodeDLL)
    {
        game_update_and_render *GameUpdateAndRender =
            (game_update_and_render *)GetProcAddress(gameCodeDLL, "GameUpdateAndRender");
        if(GameUpdateAndRender)
        {
            gameFunctions.GameUpdateAndRender = GameUpdateAndRender;
        }

        result = 1;
    }

    return result;
}

// NOTE: Width and Height are constant. Is this going to be the case
// always?
void
Win32CreateWindowsBuffer(HWND window)
{
    windowsBuffer.width = REF_WIDTH;
    windowsBuffer.height = REF_HEIGHT;
    windowsBuffer.bytesPerPixel = 4;

    BITMAPINFO *info = &windowsBuffer.bitmapInfo;
    info->bmiHeader = {};
    info->bmiHeader.biSize = sizeof(info->bmiHeader);
    info->bmiHeader.biWidth = windowsBuffer.width;
    info->bmiHeader.biHeight = windowsBuffer.height;
    info->bmiHeader.biPlanes = 1;
    info->bmiHeader.biBitCount = 32;
    info->bmiHeader.biCompression = BI_RGB;
}

void
Win32RenderBuffer(HDC deviceContext)
{
    int64 result = StretchDIBits(deviceContext,
                                 0, 0,
                                 windowsBuffer.bitmapInfo.bmiHeader.biWidth,
                                 windowsBuffer.bitmapInfo.bmiHeader.biHeight,
                                 0, 0,
                                 windowsBuffer.bitmapInfo.bmiHeader.biWidth,
                                 windowsBuffer.bitmapInfo.bmiHeader.biHeight,
                                 windowsBuffer.memory,
                                 &windowsBuffer.bitmapInfo,
                                 DIB_RGB_COLORS, SRCCOPY);
    if(result == 0)
    {
        // TODO: Logging.
    }
}

void
Win32GetClientDimensions(HWND window, Win32Dimensions *dims)
{
    RECT rect;
    GetClientRect(window, &rect);
    windowDims.leftX = rect.left;
    windowDims.topY = rect.top;
    windowDims.width = rect.right - rect.left;
    windowDims.height = rect.bottom - rect.top;
}

#define XINPUT_GET_STATE(name) DWORD name(DWORD index, XINPUT_STATE *state)
typedef XINPUT_GET_STATE(xinput_get_state);
DWORD
XInputGetStateStub(DWORD index, XINPUT_STATE *state)
{
    return ERROR_DEVICE_NOT_CONNECTED;
}
xinput_get_state *XInputGetState_ = XInputGetStateStub;
#define XInputGetState XInputGetState_

#define XINPUT_SET_STATE(name) DWORD name(DWORD index, XINPUT_VIBRATION *vibration)
typedef XINPUT_SET_STATE(xinput_set_state);
DWORD
XInputSetStateStub(DWORD index, XINPUT_VIBRATION *vibration)
{
    return ERROR_DEVICE_NOT_CONNECTED;
}
xinput_set_state *XInputSetState_ = XInputSetStateStub;
#define XInputSetState XInputSetState_

void
Win32InitXInput()
{
    HMODULE xinput = LoadLibraryA("xinput1_4.dll");
    if(!xinput)
    {
        xinput = LoadLibraryA("xinput1_3.dll");
    }

    if(xinput)
    {
        XInputGetState = (xinput_get_state *)GetProcAddress(xinput, "XInputGetState");
        XInputSetState = (xinput_set_state *)GetProcAddress(xinput, "XInputSetState");
    }
}

void
Win32InitDSound(HWND window, real32 seconds, uint32 samplesPerSec)
{
    HMODULE dsound = LoadLibraryA("dsound.dll");
    if(dsound)
    {
        direct_sound_create *DirectSoundCreate = (direct_sound_create *)
            GetProcAddress(dsound, "DirectSoundCreate");

        LPDIRECTSOUND directSound;
        if(DirectSoundCreate && SUCCEEDED(DirectSoundCreate(0, &directSound, 0)))
        {
            if(SUCCEEDED(directSound->SetCooperativeLevel(window, DSSCL_PRIORITY)))
            {
                WAVEFORMATEX waveFormat = {};
                waveFormat.wFormatTag = WAVE_FORMAT_PCM;
                waveFormat.nChannels = 2;
                waveFormat.nSamplesPerSec = samplesPerSec;
                waveFormat.wBitsPerSample = 16;
                waveFormat.nBlockAlign =
                    (waveFormat.nChannels * waveFormat.wBitsPerSample) / 8;
                waveFormat.nAvgBytesPerSec =
                    (waveFormat.nSamplesPerSec * waveFormat.nBlockAlign);
                
                DSBUFFERDESC bufferDescription = {};
                bufferDescription.dwSize = sizeof(bufferDescription);
                bufferDescription.dwFlags = DSBCAPS_PRIMARYBUFFER;
                bufferDescription.guid3DAlgorithm = GUID_NULL;

                LPDIRECTSOUNDBUFFER primaryBuffer;

                if(SUCCEEDED(directSound->
                             CreateSoundBuffer(&bufferDescription, &primaryBuffer, 0)))
                {
                    OutputDebugStringA("Primary buffer created.\n");
                    DSBUFFERDESC sBufferDescription = {};
                    sBufferDescription.dwSize = sizeof(sBufferDescription);
//                    sBufferDescription.dwFlags = DSBCAPS_GLOBALFOCUS;
                    sBufferDescription.dwBufferBytes = seconds * waveFormat.nAvgBytesPerSec;
                    sBufferDescription.lpwfxFormat = &waveFormat;
                    sBufferDescription.guid3DAlgorithm = GUID_NULL;

                    if(SUCCEEDED(directSound->
                                 CreateSoundBuffer(&sBufferDescription, &secondarySoundBuffer, 0)))
                    {
                        OutputDebugStringA("Secondary buffer created.\n");
                    }
                }
            }
        }
    }
}

LRESULT CALLBACK
Win32WindowsMsgProc(HWND window,
               UINT   msg,
               WPARAM wParam,
               LPARAM lParam)
{
    
    LRESULT result = 0;
    MouseInput *mouseInput = &gameInput.mouseInput;
    //mouseInput->isPressed = 0;
    switch(msg)
    {
        case WM_ACTIVATEAPP:
        {
            OutputDebugStringA("WM_ACTIVATEAPP\n");
        }
        break;
        case WM_SIZE:
        {
            OutputDebugStringA("WM_SIZE\n");
            Win32GetClientDimensions(window, &windowDims);
            Win32CreateWindowsBuffer(window);
        }
        break;
        case WM_SYSKEYDOWN:
        case WM_SYSKEYUP:
        case WM_KEYDOWN:
        case WM_KEYUP:
        {
            uint8 totalButtons = ARRAY_COUNT(gameInput.gameButtons, GameButton);
            bool32 isDown = ((lParam & (1 << 31)) == 0);
            bool32 altDown = ((lParam & (1 << 29)) != 0);
            
            GameButton *button;
            
            bool8 isGameButton = 0;
            switch(wParam)
            {
                case VK_LEFT:
                {
                    button = &gameInput.buttons.left;
                    isGameButton = 1;
                }
                break;
                case VK_RIGHT:
                {
                    button = &gameInput.buttons.right;
                    isGameButton = 1;
                }
                break;
                case VK_UP:
                {
                    button = &gameInput.buttons.up;
                    isGameButton = 1;
                }
                break;
                case VK_DOWN:
                {
                    button = &gameInput.buttons.down;
                    isGameButton = 1;
                }
                break;
                case VK_SPACE:
                {
                    button = &gameInput.buttons.space;
                    isGameButton = 1;
                }
                break;
                case VK_F4:
                {
                    if(isDown && altDown)
                    {
                        isRunning = 0;
                    }
                }
                break;
                case VK_ESCAPE:
                {
                    isRunning = 0;
                }
                break;
            }

            if(isGameButton)
            {
                if(isDown)
                {
                    button->isDown = 1;
                }
                else
                {
                    button->isDown = 0;
                }
            }
        }
        break;
        case WM_MOUSEMOVE:
        {
            if(mouseInput->isFirst == 0)
            {
                mouseInput->isFirst = 1;
            }
            else
            {
                mouseInput->isContinued = 1;
            }
            mouseInput->newPosX = (real32)GET_X_LPARAM(lParam);
            mouseInput->newPosY = (real32)REF_HEIGHT - (real32)GET_Y_LPARAM(lParam);
        }
        break;
        case WM_MOUSELEAVE:
        {
            mouseInput->isFirst = 0;
            mouseInput->isContinued = 0;
        }
        break;
        case WM_LBUTTONDOWN:
        {
            mouseInput->isPressed = 1;
        }
        break;
        case WM_LBUTTONUP:
        {
            mouseInput->isPressed = 0;
        }
        break;
        case WM_QUIT:
        {
            OutputDebugStringA("WM_QUIT\n");
            isRunning = 0;
        }
        break;
        case WM_CLOSE:
        {
            OutputDebugStringA("WM_CLOSE\n");
            isRunning = 0;
        }
        break;
        default:
        {
            result = DefWindowProc(window, msg, wParam, lParam);
        }
        break;
    }
    return result;
}


void
PrintPerformanceCounters(GameMemory *debugGameMemory)
{
#if ZIRANG_DEBUG
    DebugCycleCounter *debugCycleCounters = debugGameMemory->debugCycleCounters;
    for(uint32 i = 0; i < CYCLE_ID_COUNT; i++)
    {
        if(debugCycleCounters[i].hits > 0)
        {
            if(debugCycleCounters[i].highestCycles < debugCycleCounters[i].cycles)
                debugCycleCounters[i].highestCycles = debugCycleCounters[i].cycles;

            if(debugCycleCounters[i].highestHits < debugCycleCounters[i].hits)
                debugCycleCounters[i].highestHits = debugCycleCounters[i].hits;

            uint64 cyclesPerHit = debugCycleCounters[i].cycles / debugCycleCounters[i].hits;
            if(debugCycleCounters[i].highestCyclesPerHit < cyclesPerHit)
                debugCycleCounters[i].highestCyclesPerHit = cyclesPerHit;
        
            char cycleBuffer[128];
            wsprintfA(cycleBuffer, "%d: [%I64d, %I64d] cy, [%d, %d] h, [%I64d, %I64d] cy/h\n", i,
                      debugCycleCounters[i].cycles, debugCycleCounters[i].highestCycles,
                      debugCycleCounters[i].hits, debugCycleCounters[i].highestHits,
                      cyclesPerHit, debugCycleCounters[i].highestCyclesPerHit);
            OutputDebugStringA(cycleBuffer);
        }
        debugCycleCounters[i].cycles = 0;
        debugCycleCounters[i].hits = 0;
    }

    OutputDebugStringA("\n");
#endif
}

int
WinMain(HINSTANCE Instance,
        HINSTANCE hInstance,
        LPSTR cmdLine,
        int cmdShow)
{
    OutputDebugStringA("\nWelcome to Zirang Valley.\n\n");
    // TODO: Error logging & handling.
    bool32 gameLoaded = Win32LoadGameCodeDLL();
    Win32InitXInput();

    if(gameLoaded)
    {
        WNDCLASSEXA windowClass = {};
        windowClass.cbSize = sizeof(windowClass);
        windowClass.style = CS_OWNDC | CS_VREDRAW | CS_HREDRAW;
        windowClass.lpfnWndProc = Win32WindowsMsgProc;
        windowClass.hInstance = Instance;
        windowClass.lpszClassName =  "ZirangClass";

        LARGE_INTEGER queryPerformanceFrequency;
        LARGE_INTEGER lastCounter;

        QueryPerformanceFrequency(&queryPerformanceFrequency);
        QueryPerformanceCounter(&lastCounter);

        if(RegisterClassExA(&windowClass))
        {
            // NOTE: 1. Creating a window of a fixed size. Look into it
            // later. 2. The apron needs to be removed later.
            HWND window = CreateWindowA(windowClass.lpszClassName,
                                        "Zirang Valley",
                                        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                                        0,
                                        0,
                                        REF_WIDTH,
                                        REF_HEIGHT,
                                        0,
                                        0,
                                        hInstance,
                                        0);
        
            if(window)
            {
                Win32ToggleFullscreen(window);

                uint32 samplesPerSec = 48000;
                uint32 bitsPerSample = 16;
                uint32 channels = 2;
                uint32 bufferByteSize = (samplesPerSec * bitsPerSample) / 8;
                real32 bufferLengthSeconds = 2;
                
                Win32InitDSound(window, bufferLengthSeconds, samplesPerSec);
                
                HDC deviceContext = GetDC(window);
                Win32CreateWindowsBuffer(window);
                isRunning = 1;

                void *writePtr1, *writePtr2;
                DWORD lockSize1, lockSize2;;
                if(SUCCEEDED(secondarySoundBuffer->Lock(0, bufferByteSize, &writePtr1, &lockSize1,
                                               &writePtr2, &lockSize2, DSBLOCK_ENTIREBUFFER)))
                {
                    uint32 totalSamples = samplesPerSec * bufferLengthSeconds;
                    uint32 waveFrequency = 100;
                    uint32 samplesPerWave = samplesPerSec / waveFrequency;
                    uint32 samplesPerHalfWave = samplesPerWave / 2;
                    uint16 amplitude = 5000;
                    uint16 *writePtr = (uint16 *)writePtr1;
                    for(uint32 i = 0; i < totalSamples; i++)
                    {
                        uint16 currentAmplitude = amplitude;
                        if(((i % samplesPerWave) / samplesPerHalfWave) == 1)
                            currentAmplitude = -amplitude;
                        *writePtr++ = currentAmplitude;
                        *writePtr++ = currentAmplitude;
                    }

                    if(SUCCEEDED(secondarySoundBuffer->Unlock(writePtr1, lockSize1, writePtr2, lockSize2)))
                    {
//                        secondarySoundBuffer->Play(0, 0, DSBPLAY_LOOPING);
                    }
                }

                GameMemory gameMemory = {};
                GameBuffer gameBuffer = {};
                PlatformFunctions platformFunctions = {};
                platformFunctions.PlatformGetMemory = PlatformGetMemory;
                platformFunctions.PlatformGetFileSize = PlatformGetFileSize;
                platformFunctions.PlatformReadFile = PlatformReadFile;

                GameTiming gameTiming = {};
                while(isRunning)
                {
                    // NOTE: Not sure if this is the best way to get mouse
                    // input.
                    MouseInput *mouseInput = &gameInput.mouseInput;
                    mouseInput->oldPosX = mouseInput->newPosX;
                    mouseInput->oldPosY = mouseInput->newPosY;
                    mouseInput->isContinued = 0;
                    //mouseInput->isPressed = 0;
                
                    TRACKMOUSEEVENT trackMouseEvent = {};
                    trackMouseEvent.cbSize = sizeof(trackMouseEvent);
                    trackMouseEvent.dwFlags = TME_LEAVE;
                    trackMouseEvent.hwndTrack = window;
                    trackMouseEvent.dwHoverTime = 0;
                    TrackMouseEvent(&trackMouseEvent);
                
                    MSG msg;
                    while(PeekMessage(&msg, window, 0, 0, PM_REMOVE))
                    {
                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    }
                    
                    gameInput.isController = 0;
                    gameInput.controller.lAnalogX = 0.0f;
                    for (DWORD i=0; i< XUSER_MAX_COUNT; i++ )
                    {
                        XINPUT_STATE controllerState = {};
                        DWORD result = XInputGetState( i, &controllerState );

                        if(result == ERROR_SUCCESS )
                        {
                            // Controller is connected
                            XINPUT_GAMEPAD *gamepad = &controllerState.Gamepad;
                            gameInput.controller.lAnalogX = gamepad->sThumbLX;
                            gameInput.isController = 1;
                        }
                        else
                        {
                            // Controller is not connected 
                        }
                    }

                    
                    // NOTE: This is the timing information. See if it can
                    // get better.                    
                    LARGE_INTEGER currentCounter;
                    if(QueryPerformanceCounter(&currentCounter) != 0)
                    {
                        LARGE_INTEGER countsElapsed;
                        countsElapsed.QuadPart = currentCounter.QuadPart -
                            lastCounter.QuadPart;
                        LARGE_INTEGER microsecondsElapsed;
                        microsecondsElapsed.QuadPart = (countsElapsed.QuadPart * 1000000) /
                            (queryPerformanceFrequency.QuadPart);
                        gameTiming.deltaTimeMs =
                            (real64)microsecondsElapsed.QuadPart / (real64)1000;
                        gameTiming.deltaTime = gameTiming.deltaTimeMs/(real64)1000;

                        lastCounter.QuadPart = currentCounter.QuadPart;

                        // TODO: Much better logging system needed.
#if 1
                        char buffer[128];
                        wsprintfA(buffer, "DeltaTime: %d\n", (int64)gameTiming.deltaTimeMs);
                        OutputDebugStringA(buffer);
#endif
                    }

                    gameBuffer.width = windowsBuffer.width;
                    gameBuffer.height = windowsBuffer.height;
                    gameBuffer.bytesPerPixel = windowsBuffer.bytesPerPixel;

                    GameData gameData = { &gameBuffer, gameTiming, &gameMemory, &gameInput,
                                          platformFunctions };
                    if(gameFunctions.GameUpdateAndRender)
                    {
                        gameFunctions.GameUpdateAndRender(gameData);
                        windowsBuffer.memory = gameBuffer.pixels;
                    }
                    Win32RenderBuffer(deviceContext);

                    PrintPerformanceCounters(&gameMemory);
                }

                ReleaseDC(window, deviceContext);
                OutputDebugStringA("Stopping\n");
            }
            else
            {
                // TODO: Logging.
            }
        }
        else
        {
            // TODO: Logging.
        }

    }
    else
    {
        OutputDebugStringA("Game code not loaded.");
    }
}
