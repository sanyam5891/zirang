#if !defined(ZIRANG_VECTOR_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#include "zirang_math.h"

struct V2;
struct V3;
struct V4;

struct V2
{
    union
    {
        struct
        {
            real32 x;
            real32 y;
        };
        struct
        {
            real32 u;
            real32 v;
        };
        real32 e[2];
    };

    operator V3();
    operator V4();
};

inline V2
v2(real32 a, real32 b)
{
    V2 result = { a, b };
    return result;
}

inline V2
v2i(int32 a, int32 b)
{
    V2 result = { (real32)a, (real32)b };
    return result;
}

inline V2
v2u(uint32 a, uint32 b)
{
    V2 result = { (real32)a, (real32)b };
    return result;
}

inline V2
v2(real32 a)
{
    V2 result = { a, a };
    return result;
}

inline V2
v2i(int32 a)
{
    real32 ar = (real32)a;
    V2 result = { ar, ar };
    return result;
}

inline V2
v2u(uint32 a)
{
    real32 ar = (real32)a;
    V2 result = { ar, ar};
    return result;
}

struct V3
{
    union
    {
        struct
        {
            real32 x;
            real32 y;
            real32 z;
        };
        real32 e[3];
    };

    operator V2();
    operator V4();
};

inline V3
v3(real32 a, real32 b, real32 c)
{
    V3 result = { a, b, c };
    return result;
}

inline V3
v3i(int32 a, int32 b, int32 c)
{
    V3 result = { (real32)a, (real32)b, (real32)c };
    return result;
}

inline V3
v3u(uint32 a, uint32 b, uint32 c)
{
    V3 result = { (real32)a, (real32)b, (real32)c };
    return result;
}

inline V3
v3(real32 a)
{
    V3 result = { a, a, a };
    return result;
}

inline V3
v3i(int32 a)
{
    real32 ar = (real32)a;
    V3 result = { ar, ar, ar };
    return result;
}

inline V3
v3u(uint32 a)
{
    real32 ar = (real32)a;
    V3 result = { ar, ar, ar };
    return result;
}

struct V4
{
    union
    {
        struct
        {
            real32 x;
            real32 y;
            real32 z;
            real32 w;
        };
        real32 e[4];
    };

    operator V2();
    operator V3();
};

inline V4
v4(real32 a, real32 b, real32 c, real32 d)
{
    V4 result = { a, b, c, d };
    return result;
}

inline V4
v4i(int32 a, int32 b, int32 c, int32 d)
{
    V4 result = { (real32)a, (real32)b, (real32)c, (real32)d };
    return result;
}

inline V4
v4u(uint32 a, uint32 b, uint32 c, uint32 d)
{
    V4 result = { (real32)a, (real32)b, (real32)c, (real32)d };
    return result;
}

inline V4
v4(real32 a)
{
    V4 result = { a, a, a, a };
    return result;
}

inline V4
v4i(int32 a)
{
    real32 ar = (real32)a;
    V4 result = { ar, ar, ar, ar };
    return result;
}

inline V4
v4u(uint32 a)
{
    real32 ar = (real32)a;
    V4 result = { ar, ar, ar, ar };
    return result;
}


V2::operator V3()
{
    return v3(x, y, 0.0f);
}

V2::operator V4()
{
    return v4(x, y, 0.0f, 0.0f);
}

V3::operator V2()
{
    return v2(x, y);
}

V3::operator V4()
{
    return v4(x, y, z, 0.0f);
}

V4::operator V2()
{
    return v2(x, y);
}

V4::operator V3()
{
    return v3(x, y, z);
}


inline V2
operator-(V2 a)
{
    V2 result = { -a.x, -a.y };
    return result;
}

inline V3
operator-(V3 a)
{
    V3 result = { -a.x, -a.y, -a.z };
    return result;
}

inline V4
operator-(V4 a)
{
    V4 result = { -a.x, -a.y, -a.z, -a.w };
    return result;
}

inline V2
operator+(V2 a, V2 b)
{
    V2 result = { a.x + b.x, a.y + b.y };
    return result;
}

inline V3
operator+(V3 a, V3 b)
{
    V3 result = {a.x + b.x, a.y + b.y, a.z + b.z};
    return result;
}

inline V3&
operator+=(V3& a, V3 b)
{
    a = {a.x + b.x, a.y + b.y, a.z + b.z};
    return a;
}

inline V4
operator+(V4 a, V4 b)
{
    V4 result = {a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w};
    return result;
}

inline V2
operator-(V2 a, V2 b)
{
    V2 result = {a.x - b.x, a.y - b.y};
    return result;
}

inline V3
operator-(V3 a, V3 b)
{
    V3 result = {a.x - b.x, a.y - b.y, a.z - b.z};
    return result;
}

inline V3&
operator-=(V3&a, V3 b)
{
    a = {(a.x - b.x), (a.y - b.y), (a.z - b.z)};
    return a;
}

inline V4
operator-(V4 a, V4 b)
{
    V4 result = {a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w};
    return result;
}

inline V2
operator*(V2 a, V2 b)
{
    V2 result = {(a.x * b.x), (a.y * b.y)};
    return result;
}

inline V3
operator*(V3 a, V3 b)
{
    V3 result = {(a.x * b.x), (a.y * b.y), (a.z * b.z)};
    return result;
}

inline V4
operator*(V4 a, V4 b)
{
    V4 result = {(a.x * b.x), (a.y * b.y), (a.z * b.z), (a.w * b.w)};
    return result;
}

inline V2
operator*(V2 a, real32 val)
{
    V2 result = {a.x * val, a.y * val};
    return result;
}

inline V2
operator*(real32 val, V2 a)
{
    V2 result = a * val;
    return result;
}

inline V3
operator*(V3 a, real32 val)
{
    V3 result = {a.x * val, a.y * val, a.z * val};
    return result;
}

inline V3
operator*(real32 val, V3 a)
{
    V3 result = a * val;
    return result;
}

inline V4
operator*(V4 a, real32 val)
{
    V4 result = {a.x * val, a.y * val, a.z * val, a.w * val};
    return result;
}

inline V4
operator*(real32 val, V4 a)
{
    V4 result = a * val;
    return result;
}

inline V2
operator/(V2 a, V2 b)
{
    V2 result = {a.x / b.x, a.y / b.y};
    return result;
}

inline V3
operator/(V3 a, V3 b)
{
    V3 result = {a.x / b.x, a.y / b.y, a.z / b.z};
    return result;
}

inline V4
operator/(V4 a, V4 b)
{
    V4 result = {a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w};
    return result;
}

inline V2
operator/(V2 a, real32 val)
{
    V2 result = {a.x / val, a.y / val};
    return result;
}

inline V3
operator/(V3 a, real32 val)
{
    V3 result = {a.x / val, a.y / val, a.z / val};
    return result;
}

inline V4
operator/(V4 a, real32 val)
{
    V4 result = {a.x / val, a.y / val, a.z / val, a.w / val};
    return result;
}

// TODO: Implement "sqrtf".
inline real32
Magnitude(V2 a)
{
    real32 result = sqrtf((a.x * a.x) + (a.y * a.y));
    return result;
}

// TODO: Implement "sqrtf".
inline real32
Magnitude(V3 a)
{
    real32 result = sqrtf((a.x * a.x) + (a.y * a.y) + (a.z * a.z));
    return result;
}

inline real32
DegToRad(real32 deg)
{
    real32 result = (deg * PI) / 180.0f;
    return result;
}

inline real32
RadToDeg(real32 rad)
{
    real32 result = (rad * 180.0f) / PI;
    return result;
}

inline real32
Distance(V2 a, V2 b)
{
    real32 result = Magnitude(a-b);
    return result;
}

inline real32
Distance(V3 a, V3 b)
{
    real32 result = Magnitude(a-b);
    return result;
}

inline V2
Normalize(V2 vec)
{
    V2 result = {};
    real32 mag = Magnitude(vec);
    if(mag == 0.0f)
    {
    }
    else
    {
        result.x = vec.x / mag;
        result.y = vec.y / mag;
    }
    return result;
}

inline V3
Normalize(V3 vec)
{
    V3 result = {};
    real32 mag = Magnitude(vec);
    if(mag == 0.0f)
    {
    }
    else
    {
        result.x = vec.x / mag;
        result.y = vec.y / mag;
        result.z = vec.z / mag;
    }
    return result;
}

inline V2
Perp2D(V2 v)
{
    V2 result = {};
    result.x = v.y;
    result.y = -v.x;
    return result;
}

inline V2
Perp2D(V3 v)
{
    V2 result = {};
    result.x = v.y;
    result.y = -v.x;
    return result;
}

inline real32
Dot(V2 a, V2 b)
{
    real32 result = (a.x * b.x) + (a.y * b.y);
    return result;
}

inline real32
Dot(V3 a, V3 b)
{
    real32 result = (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
    return result;
}

V3
Cross(V3 a, V3 b)
{
    V3 result = {};
    result.x = (a.y * b.z) - (a.z * b.y);
    result.y = (a.z * b.x) - (a.x * b.z);
    result.z = (a.x * b.y) - (a.y * b.x);
    return result;
}

real32
GetAngleDeg(V3 aNorm, V3 bNorm)
{
    real32 dot = Dot(aNorm, bNorm);
    real32 result = RadToDeg(acos(dot));
    return result;
}

#define ZIRANG_VECTOR_H
#endif
