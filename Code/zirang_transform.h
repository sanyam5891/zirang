#if !defined(ZIRANG_TRANSFORMS_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#include "zirang_matrix.h"


struct ScreenPointResult
{
    bool32 isInside;
    V3 screenPoint;
};

struct Transform
{
    V3 position;
    V3 eulerAngles;
    V3 scale;

    Transform();
    Transform(V3);
    Transform(V3, V3);
    Transform(V3, V3, V3);
};

Transform::Transform()
{
    position = v3(0.0f);
    eulerAngles = v3(0.0f);
    scale = v3(1.0f);
}

Transform::Transform(V3 p)
{
    position = p;
    eulerAngles = v3(0.0f);
    scale = v3(1.0f);
}

Transform::Transform(V3 p, V3 e)
{
    position = p;
    eulerAngles = e;
    scale = v3(1.0f);
}

Transform::Transform(V3 p, V3 e, V3 s)
{
    position = p;
    eulerAngles = e;
    scale = s;
}

M4
Transpose(M4 m)
{
    M4 result = {};
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            result.cols[i].e[j] = m.cols[j].e[i];
        }
    }
    return result;
}

M4
Translate(V3 position)
{
    M4 result = IdentityMatrix();
    result.cols[3].x = position.x;
    result.cols[3].y = position.y;
    result.cols[3].z = position.z;
    return result;
}

M4
RotateAxis(V3 axis, real32 angleDeg)
{
    M4 result = {};
    real32 angleRad = DegToRad(angleDeg);
    real32 sint = sinf(angleRad);
    real32 cost = cosf(angleRad);

    result.cols[0].x = ((axis.x * axis.x)*(1 - cost)) + (cost);
    result.cols[0].y = ((axis.x * axis.y)*(1 - cost)) + (axis.z * sint); 
    result.cols[0].z = ((axis.x * axis.z)*(1 - cost)) - (axis.y * sint);
    result.cols[0].w = 0.0f;

    result.cols[1].x = ((axis.x * axis.y)*(1 - cost)) - (axis.z * sint); 
    result.cols[1].y = ((axis.y * axis.y)*(1 - cost)) + (cost);
    result.cols[1].z = ((axis.y * axis.z)*(1 - cost)) + (axis.x * sint);
    result.cols[1].w = 0.0f;

    result.cols[2].x = ((axis.x * axis.z)*(1 - cost)) + (axis.y * sint); 
    result.cols[2].y = ((axis.y * axis.z)*(1 - cost)) - (axis.x * sint); 
    result.cols[2].z = ((axis.z * axis.z)*(1 - cost)) + (cost);
    result.cols[2].w = 0.0f;

    result.cols[3].x = 0.0f;
    result.cols[3].y = 0.0f;
    result.cols[3].z = 0.0f;
    result.cols[3].w = 1;

    return result;
}

V3
RotatePoint(V3 point, V3 axis, real32 angleDeg)
{
    M4 rm = RotateAxis(axis, angleDeg);
    V3 result = *((V3 *)&(rm * point));
    return result;
}

M4
RotateZ(real32 angle)
{
    real32 sint = sinf(DegToRad(angle));
    real32 cost = cosf(DegToRad(angle));
    M4 result = IdentityMatrix();
    result.cols[0].x = cost;
    result.cols[0].y = sint;
    result.cols[1].x = -sint;
    result.cols[1].y = cost;
    return result;
}

M4
RotateY(real32 angle)
{
    real32 sint = sinf(DegToRad(angle));
    real32 cost = cosf(DegToRad(angle));
    M4 result = IdentityMatrix();
    result.cols[0].x = cost;
    result.cols[0].z = -sint;
    result.cols[2].x = sint;
    result.cols[2].z = cost;
    return result;
}

M4
RotateX(real32 angle)
{
    real32 sint = sinf(DegToRad(angle));
    real32 cost = cosf(DegToRad(angle));
    M4 result = IdentityMatrix();
    result.cols[1].y = cost;
    result.cols[1].z = sint;
    result.cols[2].y = -sint;
    result.cols[2].z = cost;
    return result;
}

M4
Rotate(V3 eulerAngles)
{
    M4 result = IdentityMatrix();
    
    V3 axisX = { 1.0f, 0.0f, 0.0f };
    V3 axisY = { 0.0f, 1.0f, 0.0f };
    V3 axisZ = { 0.0f, 0.0f, 1.0f };

    M4 rx = RotateAxis(axisX, eulerAngles.x);
    M4 ry = RotateAxis(axisY, eulerAngles.y);
    M4 rz = RotateAxis(axisZ, eulerAngles.z);

    result = ry * rx * rz * result;

    return result;
}

M4
Scale(V3 scale)
{
    M4 result = IdentityMatrix();

    for(int32 i = 0; i < 3; i++)
    {
        result.cols[i].e[i] = scale.e[i];
    }
    return result;
}

M4
GetModelMatrix(V3 pos, V3 angles, V3 scale)
{
    M4 result = IdentityMatrix();
    M4 sm = Scale(scale);
    M4 rm = Rotate(angles);
    M4 tm = Translate(pos);
    result = tm * rm * sm * result;
    return result;
}

M4
GetModelMatrix(V3 pos, V3 angles)
{
    M4 result = IdentityMatrix();
    M4 rm = Rotate(angles);
    M4 tm = Translate(pos);
    result = tm * rm * result;
    return result;
}

M4
GetModelMatrix(V3 pos)
{
    M4 result = IdentityMatrix();
    M4 tm = Translate(pos);
    result = tm * result;
    return result;
}

M4
LookAt(V3 pos, V3 target, V3 front, V3 left)
{
    V3 wUp = { 0.0f, 1.0f, 0.0f };
    V3 up = Cross(front, left);

    M4 rotation = {};
    rotation.cols[0] = { left.x, left.y, left.z, 0.0f };
    rotation.cols[1] = { up.x, up.y, up.z, 0.0f };
    rotation.cols[2] = { front.x, front.y, front.z, 0.0f };
    rotation.cols[3] = { 0.0f, 0.0f, 0.0f, 1.0f };

    M4 result = IdentityMatrix();
    result = Translate(-pos) * result;
    result = Transpose(rotation) * result;
    return result;
}

M4
LookAt(V3 pos, V3 target, V3 scale = v3(1.0f))
{
    V3 wUp = v3(0.0f, 1.0f, 0.0f);
    V3 forward = Normalize(target - pos);
    V3 left = Cross(wUp, forward);
    V3 up = Cross(forward, left);

    M4 scaleMatrix = Scale(scale);
    M4 rotationTransposeMatrix = IdentityMatrix();
    rotationTransposeMatrix.cols[0] = { left.x, left.y, left.z, 0.0f };
    rotationTransposeMatrix.cols[1] = { up.x, up.y, up.z, 0.0f };
    rotationTransposeMatrix.cols[2] = { forward.x, forward.y, forward.z, 0.0f };
    rotationTransposeMatrix.cols[3] = { pos.x, pos.y, pos.z, 1.0f };
    M4 result = rotationTransposeMatrix * scaleMatrix;
    return result;
}

M4
Orthographic(real32 nearPlane, real32 farPlane, real32 aspectRatio, real32 cameraHeight)
{
    M4 result = {};
    real32 n = nearPlane;
    real32 f = farPlane;
    
    real32 t = cameraHeight / 2.0f;
    real32 b = -t;

    real32 r = t * aspectRatio;
    real32 l = -r;

    result.cols[0].x = 1 / r;
    result.cols[0].y = 0.0f;
    result.cols[0].z = 0.0f;
    result.cols[0].w = 0.0f;

    result.cols[1].x = 0.0f;
    result.cols[1].y = 1/ t;
    result.cols[1].z = 0.0f;
    result.cols[1].w = 0.0f;

    result.cols[2].x = 0.0f;
    result.cols[2].y = 0.0f;
    result.cols[2].z = 2.0f/(n - f);
    result.cols[2].w = 0.0f;

    result.cols[3].x = 0.0f;
    result.cols[3].y = 0.0f;
    result.cols[3].z = (f + n) / (n - f);
    result.cols[3].w = 1;

    return result;    
}

M4
Perspective(real32 nearPlane, real32 farPlane, real32 aspectRatio, real32 fov)
{
    M4 result = {};
    real32 n = nearPlane;
    real32 f = farPlane;
    
    real32 fov2 = fov * 0.5f;
    real32 fov2rad = DegToRad(fov2);
    real32 t = abs(n * tan(fov2rad));
    real32 b = -t;

    real32 r = t * aspectRatio;
    real32 l = -r;

    result.cols[0].x = (2 * n) / (r - l);
    result.cols[0].y = 0.0f;
    result.cols[0].z = (r + l) / (r - l);
    result.cols[0].w = 0.0f;

    result.cols[1].x = 0.0f;
    result.cols[1].y = (2 * n) / (t - b);
    result.cols[1].z = (t + b) / (t - b);
    result.cols[1].w = 0.0f;
    
    result.cols[2].x = 0.0f;
    result.cols[2].y = 0.0f;
    result.cols[2].z = (n + f)/(n - f);
    result.cols[2].w = -1;

    result.cols[3].x = 0.0f;
    result.cols[3].y = 0.0f;
    result.cols[3].z = (-2 * f * n) / (f - n);
    result.cols[3].w = 0.0f;
    
    return result;
}

V3
Viewport(V3 ndc, real32 left, real32 bottom, real32 right, real32 top)
{
    V3 result = {};
    real32 width2 = (right - left) / 2.0f;
    real32 height2 = (top - bottom) / 2.0f;
    result.x = ((left + right)/2.0f) + (width2 * ndc.x);
    result.y = ((bottom + top)/2.0f) + (height2 * ndc.y);
    result.z = ndc.z;
    return result;
}

ScreenPointResult
GetScreenCoordinates(V4 preClippedCoordinates, uint32 width, uint32  height)
{
    V3 screenPointNdc = { preClippedCoordinates.x / preClippedCoordinates.w,
                            preClippedCoordinates.y / preClippedCoordinates.w,
                            preClippedCoordinates.z / preClippedCoordinates.w };

    ScreenPointResult result = {};
    if(screenPointNdc.x > -1.0f && screenPointNdc.x < 1.0f &&
       screenPointNdc.y > -1.0f && screenPointNdc.y < 1.0f &&
       screenPointNdc.z > -1.0f && screenPointNdc.z < 1.0f)
    {
        result.isInside = true;
    }
    result.screenPoint = Viewport(screenPointNdc, 0.0f, 0.0f, width, height);
    return result;    
}

ScreenPointResult
GetScreenCoordinates(M4 pvm, V3 localVert,
                     uint32 width, uint32 height)
{
    V4 localVert4 = { localVert.x, localVert.y, localVert.z, 1.0f };
    V4 preClippedCoordinates = pvm * localVert4;

    ScreenPointResult result = GetScreenCoordinates(preClippedCoordinates, width, height);
    return result;
}

V3
GetNDC(M4 pvm, V3 localVert,
       uint32 width, uint32 height)
{
    V4 localVert4 = { localVert.x, localVert.y, localVert.z, 1.0f };
    V4 screenPoint4 = pvm * localVert4;

    V3 result = { screenPoint4.x / screenPoint4.w,
                    screenPoint4.y / screenPoint4.w,
                    screenPoint4.z / screenPoint4.w };

    return result;
}

#define ZIRANG_TRANSFORMS_H
#endif
