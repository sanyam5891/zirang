
#if !defined(ZIRANG_COLOR_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

struct Color;
//struct Color32;

inline Color
ClampColor(Color c);

//inline Color32
//ClampColor(Color32 c);

struct Color
{
    union
    {
        struct
        {
            real32 r;
            real32 g;
            real32 b;
            real32 a;
        };

        struct
        {
            real32 v[4];
        };
    };

    Color();
    Color(real32);
    Color(real32, real32); 
    Color(real32, real32, real32, real32);

    //operator Color32();
    operator uint32();
};

Color::Color()
{
    r = 0.0f;
    g = 0.0f;
    b = 0.0f;
    a = 1.0f;
}

Color::Color(real32 value)
{
    r = value;
    g = value;
    b = value;
    a = value;
}

Color::Color(real32 colorValue, real32 alpha)
{
    r = colorValue;
    g = colorValue;
    b = colorValue;
    a = alpha;
};

Color::Color(real32 red, real32 green, real32 blue, real32 alpha)
{
    r = red;
    g = green;
    b = blue;
    a = alpha;
}

/*
struct Color32
{
    uint8 r;
    uint8 g;
    uint8 b;
    uint8 a;

    Color32();
    Color32(uint8);
    Color32(uint8, uint8);
    Color32(uint8, uint8, uint8, uint8);

    operator Color();
    operator uint32();
};

Color32::Color32()
{
    r = 0;
    g = 0;
    b = 0;
    a = 255;
}

Color32::Color32(uint8 value)
{
    r = value;
    g = value;
    b = value;
    a = value;
}

Color32::Color32(uint8 colorValue, uint8 alpha)
{
    r = colorValue;
    g = colorValue;
    b = colorValue;
    a = alpha;
}

Color32::Color32(uint8 red, uint8 green, uint8 blue, uint8 alpha)
{
    r = red;
    g = green;
    b = blue;
    a = alpha;
}

// NOTE: Typecast operators.

Color::operator Color32()
{
    uint8 red = (uint8)Clamp((uint8)(r * 255.0f), 0, 255);
    uint8 green = (uint8)Clamp((uint8)(g * 255.0f), 0, 255);
    uint8 blue = (uint8)Clamp((uint8)(b * 255.0f), 0, 255);
    uint8 alpha = (uint8)Clamp((uint8)(a * 255.0f), 0, 255);
    Color32 color32 = { red, green, blue, alpha };
    return color32;
}
*/
Color::operator uint32()
{
    uint8 red = (uint8)Roundf(Clamp(r, 0.0f, 1.0f) * 255.0f);
    uint8 green = (uint8)Roundf(Clamp(g, 0.0f, 1.0f) * 255.0f);
    uint8 blue = (uint8)Roundf(Clamp(b, 0.0f, 1.0f) * 255.0f);
    uint8 alpha = (uint8)Roundf(Clamp(a, 0.0f, 1.0f) * 255.0f);

    uint32 result = (alpha << 24) | (red << 16) | (green << 8) | (blue); // ARGB
    return result;
}
/*
Color32::operator Color()
{
    Color result = { r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f };
    return result;  
}

Color32::operator uint32()
{
    uint32 result = (a << 24) | (r << 16) | (g << 8) | (b); // ARGB
    return result;
}
*/

// NOTE: Arithmetic operators.

Color
operator*(real32 val, Color c)
{
    Color result = Color(val * c.r, val * c.g, val * c.b, val * c.a);
    result = ClampColor(result);
    return result;
}

Color
operator+(Color c0, Color c1)
{
    Color result = Color(c0.r + c1.r, c0.g + c1.g, c0.b + c1.b, c0.a + c1.a);
    result = ClampColor(result);
    return result;
}

Color
operator-(Color c0, Color c1)
{
    Color result = Color(c0.r - c1.r, c0.g - c1.g, c0.b - c1.b, c0.a - c1.a);
    result = ClampColor(result);
    return result;
}

Color
operator*(Color c0, Color c1)
{
    Color result = Color(c0.r * c1.r, c0.g * c1.g, c0.b * c1.b, c0.a * c1.a);
    result = ClampColor(result);
    return result;
}
/*
Color32
operator*(real32 val, Color32 c)
{
    Color32 result = (Color32)(val * (Color)c);
    return result;
}

Color32
operator+(Color32 c0, Color32 c1)
{
    Color32 result = Color32((Color)c0 + (Color)c1);
    return result;
}

Color32
operator-(Color32 c0, Color32 c1)
{
    Color result = Color32((Color)c0 - (Color)c1);
    return result;
}

Color32
operator*(Color32 c0, Color32 c1)
{
    Color32 result = (Color32)((Color)c0 * (Color)c1);
    return result;
}
*/

// NOTE: Utility functions.

inline Color
ClampColor(Color c)
{
    Color result = c;
    result.r = Clamp(result.r, 0.0f, 1.0f);
    result.g = Clamp(result.g, 0.0f, 1.0f);
    result.b = Clamp(result.b, 0.0f, 1.0f);
    result.a = Clamp(result.a, 0.0f, 1.0f);
    return result;
}
/*
inline Color32
ClampColor(Color32 c)
{
    Color32 result = c;
    result.r = Clamp(result.r, 0, 255);
    result.g = Clamp(result.g, 0, 255);
    result.b = Clamp(result.b, 0, 255);
    result.a = Clamp(result.a, 0, 255);
    return result;
}
*/
inline Color
LerpColor(Color c0, Color c1, real32 t)
{
    real32 oneMinusT = 1.0f - t;
    Color result = (t * c0) + (oneMinusT * c1);
    return result;
}
/*
inline Color32
LerpColor(Color32 c0, Color32 c1, real32 t)
{
    real32 oneMinusT = 1.0f - t;
    Color32 result = (t * c0) + (oneMinusT * c1);
    return result;
}
*/
inline Color
U32ToColor(uint32 c)
{
    real32 a = ((real32)((c & 0xff000000) >> 24)) / 255.0f;
    real32 r = ((real32)((c & 0x00ff0000) >> 16)) / 255.0f;
    real32 g = ((real32)((c & 0x0000ff00) >> 8)) / 255.0f;
    real32 b = ((real32)((c & 0x000000ff))) / 255.0f;
    Color result = { r, g, b, a };
    return result;
}
/*
inline Color32
U32ToColor32(uint32 c)
{
    Color32 result = (Color32)(U32ToColor(c));
    return result;
}
*/
#define ZIRANG_COLOR_H
#endif
