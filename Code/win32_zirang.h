#if !defined(WIN32_ZIRANG_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

struct Win32WindowsBuffer
{
    BITMAPINFO bitmapInfo;
    void *memory;
    void *zBuffer;
    uint16 width;
    uint16 height;
    uint16 bytesPerPixel;
};

struct Win32Dimensions
{
    uint32 leftX;
    uint32 topY;
    uint16 width;
    uint16 height;
};

#define WIN32_ZIRANG_H
#endif
