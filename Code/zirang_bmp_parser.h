#if !defined(ZIRANG_BMP_PARSER_H)
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Sanyam Malhotra $
   $Notice: Open source. $
   ======================================================================== */

#pragma pack(push,1)
struct BitmapFileHeader
{
    uint16 type;
    uint32 size;
    uint16 reserved1;
    uint16 reserved2;
    uint32 byteOffset;
};

struct BitmapInfoHeader
{
  uint32 size;
  int32  width;
  int32  height;
  uint16 planes;
  uint16 bitsPerPixel;
  uint32 compression;
  uint32 imageSize;
  int32  xPixelsPerMeter;
  int32  yPixelsPerMeter;
  uint32 clrUsed;
  uint32 clrImportant;
  uint32 redMask;
  uint32 greenMask;
  uint32 blueMask;
  uint32 alphaMask;
    /*
  uint32        bV4CSType;
  CIEXYZTRIPLE bV4Endpoints;
  uint32        bV4GammaRed;
  uint32        bV4GammaGreen;
  uint32        bV4GammaBlue;
    */
};

struct BitmapData
{
    BitmapFileHeader fileHeader;
    BitmapInfoHeader infoHeader;
};
#pragma pack(pop)

LoadedBitmap
GetLoadedBitmap(uint32 size, void *data, MemoryArena *arena)
{
    LoadedBitmap result = {};
    BitmapData *bitmapData = (BitmapData *)data;

    // IMPORTANT: This doesn't take into account top-down bitmap.
    // NOTE: 3 here is BI_BITFIELDS.
    if(bitmapData->infoHeader.compression != 3 || bitmapData->infoHeader.bitsPerPixel != 32)
    {
            Assert(0);
    }
    else
    {
        result.width = bitmapData->infoHeader.width;
        result.height = bitmapData->infoHeader.height;
        result.bitsPerPixel = bitmapData->infoHeader.bitsPerPixel;
        uint32 *colors = (uint32 *)(((uint8 *)data) + bitmapData->fileHeader.byteOffset);

        uint32 totalBytes = result.width * result.height * (result.bitsPerPixel / 8);
        result.data = (Color *)PushBuffer(arena, totalBytes);
        uint32 *current = (uint32 *)result.data;
        // Final memory layout -> RGBA
        // Input value -> 0xABGR
        // BMP's memory layout -> ABGR
        // Value picked up -> 0xRGBA
        uint32 alphaMask = bitmapData->infoHeader.alphaMask;
        uint32 redMask = bitmapData->infoHeader.redMask;
        uint32 greenMask = bitmapData->infoHeader.greenMask;
        uint32 blueMask = bitmapData->infoHeader.blueMask;

        int32 alphaFSBP = GetFirstSetBitPosition(alphaMask);
        int32 redFSBP = GetFirstSetBitPosition(redMask);
        int32 greenFSBP = GetFirstSetBitPosition(greenMask);
        int32 blueFSBP = GetFirstSetBitPosition(blueMask);

        if(alphaFSBP == -1 || redFSBP == -1 || greenFSBP == -1 || blueFSBP == -1)
        {
            Assert(0);
        }
        else
        {
            for(uint32 row = 0; row < result.width; row++)
            {
                for(uint32 col = 0; col < result.height; col++)
                {
                    uint32 color = *colors++;
                    uint32 redShifted = ((color & redMask) >> redFSBP);
                    uint32 greenShifted = ((color & greenMask) >> greenFSBP) << 8;
                    uint32 blueShifted = ((color & blueMask) >> blueFSBP) << 16;
                    uint32 alphaShifted = ((color & alphaMask) >> alphaFSBP) << 24;
                    uint32 outputColor = (redShifted | greenShifted | blueShifted | alphaShifted);
                    *current++ = outputColor;
                }
            }
        }
    }

    return result;
}

#define ZIRANG_BMP_PARSER_H
#endif
