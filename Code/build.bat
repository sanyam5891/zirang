@echo off
set CommonCompilerFlags= -wd4838 -wd4172 -wd4005 -Od -Zi -DZIRANG_DEBUG=1
set CommonLinkerFlags= -incremental:no -opt:ref user32.lib gdi32.lib opengl32.lib
pushd..
pushd..
mkdir ZirangBuild
pushd ZirangBuild
cl %CommonCompilerFlags% w:\Zirang\Code\zirang.cpp -Fmzirang.map -LD /link -incremental:no -opt:ref -EXPORT:GameUpdateAndRender
cl %CommonCompilerFlags% w:\Zirang\Code\win32_zirang.cpp -Fmwin32_zirang.map /link %CommonLinkerFlags%
popd
popd
popd
